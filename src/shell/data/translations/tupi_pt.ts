<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_PT">
<context>
    <name>ButtonsPanel</name>
    <message>
        <location filename="../src/plugins/tools/common/buttonspanel.cpp" line="41"/>
        <source>Edit Tween</source>
        <translation>Editar Intermeios</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/common/buttonspanel.cpp" line="45"/>
        <source>Remove Tween</source>
        <translation>Remover Intermeios</translation>
    </message>
</context>
<context>
    <name>Configurator</name>
    <message>
        <location filename="../src/plugins/tools/color/configurator.cpp" line="66"/>
        <source>Coloring Tween</source>
        <translation>Intermeios de Cor</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/configurator.cpp" line="71"/>
        <source>Compound Tween</source>
        <translation>Intermeios Composto</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/ink/configurator.cpp" line="58"/>
        <location filename="../src/plugins/tools/scheme/configurator.cpp" line="52"/>
        <source>Parameters</source>
        <translation>Parâmetros</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/ink/configurator.cpp" line="80"/>
        <location filename="../src/plugins/tools/scheme/configurator.cpp" line="72"/>
        <source>Dot Spacing</source>
        <translation>Espaço entre Pontos</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/ink/configurator.cpp" line="96"/>
        <location filename="../src/plugins/tools/scheme/configurator.cpp" line="88"/>
        <source>Size Tolerance</source>
        <translation>Tolerância de Tamanho</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/ink/configurator.cpp" line="121"/>
        <location filename="../src/plugins/tools/scheme/configurator.cpp" line="112"/>
        <source>Smoothness</source>
        <translation>Suavidade</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/opacity/configurator.cpp" line="70"/>
        <source>Opacity Tween</source>
        <translation>Intermeios de Opacidade</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/position/configurator.cpp" line="80"/>
        <source>Position Tween</source>
        <translation>Intermeios de Posição</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/configurator.cpp" line="70"/>
        <location filename="../src/plugins/tools/rotation/configurator.cpp" line="70"/>
        <source>Rotation Tween</source>
        <translation>Intermeios de Rotação</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scale/configurator.cpp" line="70"/>
        <source>Scale Tween</source>
        <translation>Intermeios de Escala</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scheme/configurator.cpp" line="48"/>
        <source>This tool is just a &lt;b&gt;proof-of-concept&lt;/b&gt; of the basic algorithm for the Tupi&apos;s free-tracing vectorial brushes</source>
        <translation>Esta ferramenta é apenas uma &lt;b&gt;prova-de-conceito&lt;/b&gt;do algoritmo básico para os pincéis vetoriais do Tupi</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scheme/configurator.cpp" line="58"/>
        <source>Structure</source>
        <translation>Estrutura</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scheme/configurator.cpp" line="63"/>
        <source>Basic</source>
        <translation>Básico</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scheme/configurator.cpp" line="64"/>
        <source>Axial</source>
        <translation>Axial</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scheme/configurator.cpp" line="65"/>
        <source>Organic</source>
        <translation>Orgânico</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scheme/configurator.cpp" line="104"/>
        <source>Run simulation</source>
        <translation>Executar simulação</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/shear/configurator.cpp" line="70"/>
        <source>Shear Tween</source>
        <translation>Intermeios de Corte</translation>
    </message>
</context>
<context>
    <name>EraserTool</name>
    <message>
        <location filename="../src/plugins/tools/eraser/erasertool.cpp" line="49"/>
        <location filename="../src/plugins/tools/eraser/erasertool.cpp" line="54"/>
        <location filename="../src/plugins/tools/eraser/erasertool.cpp" line="58"/>
        <location filename="../src/plugins/tools/eraser/erasertool.cpp" line="79"/>
        <source>Eraser</source>
        <translation>Borracha</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/eraser/erasertool.cpp" line="55"/>
        <source>E</source>
        <translation>E</translation>
    </message>
</context>
<context>
    <name>ExportTo</name>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="469"/>
        <source>Image name prefix: </source>
        <translation>Prefixo do nome da imagem: </translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="477"/>
        <source>File: </source>
        <translation>Arquivo: </translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="474"/>
        <source>Directory: </source>
        <translation>Diretório: </translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="459"/>
        <source>Enable background transparency</source>
        <translation>Habilitar transparência do fundo</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="516"/>
        <source>i.e. &lt;B&gt;%1&lt;/B&gt;01.png / &lt;B&gt;%1&lt;/B&gt;01.jpg</source>
        <translation>ex. &lt;B&gt;%1&lt;/B&gt;01.png / &lt;B&gt;%1&lt;/B&gt;01.jpg</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="530"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="541"/>
        <source>Configuration</source>
        <translation>Configuração</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="555"/>
        <source>FPS</source>
        <translation>FPS</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="649"/>
        <source>Choose a file name...</source>
        <translation>Escolha um nome de arquivo...</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="663"/>
        <source>Choose a directory...</source>
        <translation>Escolha um diretório...</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="741"/>
        <source>Warning!</source>
        <translation>Alerta!</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="742"/>
        <source>File exists. Overwrite it?</source>
        <translation>O arquivo existe. Deseja sobrescrevê-lo?</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="698"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="711"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="752"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="767"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="818"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="698"/>
        <source>Images name prefix can&apos;t be empty! Please, type a prefix.</source>
        <translation>O prefixo do nome das imagens não pode ser vazio! Por favor, digite um prefixo.</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="711"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="752"/>
        <source>Directory doesn&apos;t exist! Please, choose another path.</source>
        <translation>Diretório não existe! Por favor, escolha outro caminho.</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="767"/>
        <source>You have no permission to create this file. Please, choose another path.</source>
        <translation>Você não tem permissão para criar este arquivo. Por favor, escolha outro caminho.</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="818"/>
        <source>Format problem. Tupi Internal error.</source>
        <translation>Problema no formato. Erro Interno do Tupi.</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="825"/>
        <source>Information</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="829"/>
        <source>ERROR!</source>
        <translation>ERRO!</translation>
    </message>
</context>
<context>
    <name>FillTool</name>
    <message>
        <location filename="../src/plugins/tools/fill/filltool.cpp" line="75"/>
        <location filename="../src/plugins/tools/fill/filltool.cpp" line="80"/>
        <location filename="../src/plugins/tools/fill/filltool.cpp" line="84"/>
        <location filename="../src/plugins/tools/fill/filltool.cpp" line="177"/>
        <location filename="../src/plugins/tools/fill/filltool.cpp" line="278"/>
        <source>Internal fill</source>
        <translation>Preenchimento interno</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/fill/filltool.cpp" line="75"/>
        <location filename="../src/plugins/tools/fill/filltool.cpp" line="86"/>
        <location filename="../src/plugins/tools/fill/filltool.cpp" line="90"/>
        <location filename="../src/plugins/tools/fill/filltool.cpp" line="179"/>
        <location filename="../src/plugins/tools/fill/filltool.cpp" line="280"/>
        <source>Line fill</source>
        <translation>Preenchimento de Contorno</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/fill/filltool.cpp" line="81"/>
        <source>I</source>
        <translation>I</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/fill/filltool.cpp" line="87"/>
        <source>B</source>
        <translation>B</translation>
    </message>
</context>
<context>
    <name>GeometricTool</name>
    <message>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="82"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="113"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="118"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="149"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="207"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="209"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="222"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="285"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="308"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="339"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="386"/>
        <source>Rectangle</source>
        <translation>Retângulo</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="82"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="120"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="125"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="156"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="207"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="311"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="341"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="388"/>
        <source>Ellipse</source>
        <translation>Elípse</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="82"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="127"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="132"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="163"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="315"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="390"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="460"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="467"/>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="474"/>
        <source>Line</source>
        <translation>Linha</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="114"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="121"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/geometric/geometrictool.cpp" line="128"/>
        <source>L</source>
        <translation>L</translation>
    </message>
</context>
<context>
    <name>ImagePlugin</name>
    <message>
        <location filename="../src/plugins/export/imageplugin/imageplugin.cpp" line="48"/>
        <source>Image Array</source>
        <translation type="unfinished">Sequência de Imagens</translation>
    </message>
</context>
<context>
    <name>InfoPanel</name>
    <message>
        <location filename="../src/plugins/tools/geometric/infopanel.cpp" line="43"/>
        <location filename="../src/plugins/tools/polyline/infopanel.cpp" line="51"/>
        <source>Tips</source>
        <translation>Dicas</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/geometric/infopanel.cpp" line="55"/>
        <source>Close the line path</source>
        <translation>Fechar o caminho da linha</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/geometric/infopanel.cpp" line="55"/>
        <source>Mouse Right Click</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/geometric/infopanel.cpp" line="56"/>
        <source>Shift</source>
        <translation>Deslocamento</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/geometric/infopanel.cpp" line="56"/>
        <source>Align line to horizontal/vertical axis</source>
        <translation>Alinhar linha ao eixo horizontal/vertical</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/geometric/infopanel.cpp" line="58"/>
        <source>Shift + Left Mouse Button</source>
        <translation>Shift + Botão Esquerdo do Mouse</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/geometric/infopanel.cpp" line="58"/>
        <source>Set width/height proportional dimensions</source>
        <translation>Definir dimensões de largura/altura proporcionais</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/polyline/infopanel.cpp" line="59"/>
        <source>Close line</source>
        <translation>Fechar linha</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/polyline/infopanel.cpp" line="59"/>
        <source>X key or Right mouse button</source>
        <translation>Tecla X ou Botão direito do mouse</translation>
    </message>
</context>
<context>
    <name>InkTool</name>
    <message>
        <location filename="../src/plugins/tools/ink/inktool.cpp" line="127"/>
        <location filename="../src/plugins/tools/ink/inktool.cpp" line="605"/>
        <location filename="../src/plugins/tools/ink/inktool.cpp" line="609"/>
        <source>Ink</source>
        <translation>Tinta</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/ink/inktool.cpp" line="606"/>
        <source>K</source>
        <translation>K</translation>
    </message>
</context>
<context>
    <name>KinasWidget</name>
    <message>
        <location filename="../src/components/kinas/kinaswidget.cpp" line="42"/>
        <source>Tupi Script</source>
        <translation>Tupi Script</translation>
    </message>
</context>
<context>
    <name>NodesTool</name>
    <message>
        <location filename="../src/plugins/tools/nodes/nodestool.cpp" line="88"/>
        <location filename="../src/plugins/tools/nodes/nodestool.cpp" line="351"/>
        <location filename="../src/plugins/tools/nodes/nodestool.cpp" line="354"/>
        <source>Nodes Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/nodes/nodestool.cpp" line="352"/>
        <source>N</source>
        <translation type="unfinished">N</translation>
    </message>
</context>
<context>
    <name>PapagayoTool</name>
    <message>
        <location filename="../src/plugins/tools/papagayo/papagayotool.cpp" line="134"/>
        <location filename="../src/plugins/tools/papagayo/papagayotool.cpp" line="257"/>
        <location filename="../src/plugins/tools/papagayo/papagayotool.cpp" line="261"/>
        <location filename="../src/plugins/tools/papagayo/papagayotool.cpp" line="555"/>
        <source>Papagayo Lip-sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/papagayotool.cpp" line="259"/>
        <source>Shift+R</source>
        <translation type="unfinished">Shift+R</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/papagayotool.cpp" line="427"/>
        <source>Error</source>
        <translation type="unfinished">Erro</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/papagayotool.cpp" line="427"/>
        <source>Tween name is missing!</source>
        <translation type="unfinished">Nome do intermeios indefinido!</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/papagayotool.cpp" line="533"/>
        <source>Frame %1</source>
        <translation type="unfinished">Quadro %1</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/papagayotool.cpp" line="544"/>
        <source>Info</source>
        <translation type="unfinished">Informação</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/papagayotool.cpp" line="544"/>
        <source>Tween %1 applied!</source>
        <translation type="unfinished">Intermeios %1 aplicado!</translation>
    </message>
</context>
<context>
    <name>PencilTool</name>
    <message>
        <location filename="../src/plugins/tools/pencil/penciltool.cpp" line="78"/>
        <location filename="../src/plugins/tools/pencil/penciltool.cpp" line="83"/>
        <location filename="../src/plugins/tools/pencil/penciltool.cpp" line="129"/>
        <source>Pencil</source>
        <translation>Lápis</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/pencil/penciltool.cpp" line="79"/>
        <source>P</source>
        <translation>P</translation>
    </message>
</context>
<context>
    <name>PolyLineTool</name>
    <message>
        <location filename="../src/plugins/tools/polyline/polylinetool.cpp" line="93"/>
        <location filename="../src/plugins/tools/polyline/polylinetool.cpp" line="97"/>
        <location filename="../src/plugins/tools/polyline/polylinetool.cpp" line="122"/>
        <source>PolyLine</source>
        <translation type="unfinished">Polilinha</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/polyline/polylinetool.cpp" line="94"/>
        <source>S</source>
        <translation type="unfinished">S</translation>
    </message>
</context>
<context>
    <name>PositionSettings</name>
    <message>
        <location filename="../src/plugins/tools/compound/positionsettings.cpp" line="68"/>
        <source>Component</source>
        <translation>Componente</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/positionsettings.cpp" line="71"/>
        <source>Position</source>
        <translation>Posição</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/positionsettings.cpp" line="82"/>
        <source>Starting at frame</source>
        <translation>Iniciar no quadro</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/positionsettings.cpp" line="109"/>
        <location filename="../src/plugins/tools/compound/positionsettings.cpp" line="150"/>
        <location filename="../src/plugins/tools/compound/positionsettings.cpp" line="170"/>
        <location filename="../src/plugins/tools/compound/positionsettings.cpp" line="202"/>
        <location filename="../src/plugins/tools/compound/positionsettings.cpp" line="233"/>
        <source>Frames Total</source>
        <translation>Total de Quadros</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/positionsettings.cpp" line="154"/>
        <source>Cancel Tween</source>
        <translation>Cancelar Intermeios</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/positionsettings.cpp" line="216"/>
        <source>Info</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/positionsettings.cpp" line="216"/>
        <source>You must define a path for this Tween!</source>
        <translation>Você deve definir um caminho para este Intermeios!</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/positionsettings.cpp" line="225"/>
        <source>Position Tween is set!</source>
        <translation>Intermeios de Posição está definido!</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/positionsettings.cpp" line="250"/>
        <source>Close Tween properties</source>
        <translation>Fechar propriedades de Intermeios</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/framework/tcore/tdebug.cpp" line="229"/>
        <source>Information</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../src/framework/tcore/tdebug.cpp" line="235"/>
        <source>Warning</source>
        <translation>Alerta</translation>
    </message>
    <message>
        <location filename="../src/framework/tcore/tdebug.cpp" line="241"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/framework/tcore/tdebug.cpp" line="247"/>
        <source>Critical</source>
        <translation>Crítico</translation>
    </message>
    <message>
        <location filename="../src/shell/tupcrashhandler.cpp" line="51"/>
        <source>Fatal Error</source>
        <translation>Erro Fatal</translation>
    </message>
    <message>
        <location filename="../src/shell/tupcrashhandler.cpp" line="52"/>
        <source>Well, Tupi has crashed...</source>
        <translation>Bem, o Tupi falhou...</translation>
    </message>
    <message>
        <location filename="../src/shell/tupcrashhandler.cpp" line="53"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../src/shell/tupcrashhandler.cpp" line="54"/>
        <source>Re-launch Tupi</source>
        <translation>Recarregar o Tupi</translation>
    </message>
    <message>
        <location filename="../src/shell/tupcrashhandler.cpp" line="55"/>
        <source>This is a general failure</source>
        <translation>Esta é uma falha geral</translation>
    </message>
    <message>
        <location filename="../src/store/tupprojectcommand.cpp" line="159"/>
        <source>add</source>
        <translation>adicionar</translation>
    </message>
    <message>
        <location filename="../src/store/tupprojectcommand.cpp" line="164"/>
        <source>remove</source>
        <translation>remover</translation>
    </message>
    <message>
        <location filename="../src/store/tupprojectcommand.cpp" line="169"/>
        <source>move</source>
        <translation>mover</translation>
    </message>
    <message>
        <location filename="../src/store/tupprojectcommand.cpp" line="174"/>
        <source>lock</source>
        <translation>travar</translation>
    </message>
    <message>
        <location filename="../src/store/tupprojectcommand.cpp" line="179"/>
        <source>rename</source>
        <translation>renomear</translation>
    </message>
    <message>
        <location filename="../src/store/tupprojectcommand.cpp" line="184"/>
        <source>select</source>
        <translation>selecionar</translation>
    </message>
    <message>
        <location filename="../src/store/tupprojectcommand.cpp" line="189"/>
        <source>edit node</source>
        <translation>editar nó</translation>
    </message>
    <message>
        <location filename="../src/store/tupprojectcommand.cpp" line="194"/>
        <source>view</source>
        <translation>ver</translation>
    </message>
    <message>
        <location filename="../src/store/tupprojectcommand.cpp" line="199"/>
        <source>transform</source>
        <translation>transformar</translation>
    </message>
    <message>
        <location filename="../src/store/tupprojectcommand.cpp" line="204"/>
        <source>convert</source>
        <translation>converter</translation>
    </message>
    <message>
        <location filename="../src/net/tupnetprojectmanagerhandler.cpp" line="535"/>
        <source>Wall from</source>
        <translation>Muro a partir de</translation>
    </message>
</context>
<context>
    <name>SchemeTool</name>
    <message>
        <location filename="../src/plugins/tools/scheme/schemetool.cpp" line="129"/>
        <location filename="../src/plugins/tools/scheme/schemetool.cpp" line="682"/>
        <location filename="../src/plugins/tools/scheme/schemetool.cpp" line="686"/>
        <source>Scheme</source>
        <translation>Esquema</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scheme/schemetool.cpp" line="683"/>
        <source>M</source>
        <translation>M</translation>
    </message>
</context>
<context>
    <name>SelectPlugin</name>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="77"/>
        <source>Select plugin</source>
        <translation>Selecionar plugin</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="142"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="208"/>
        <source>WEBM Video</source>
        <translation>Vídeo WEBM</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="147"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="211"/>
        <source>OGV Video</source>
        <translation>Vídeo OGM</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="152"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="214"/>
        <source>MPEG Video</source>
        <translation>Vídeo MPEG</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="157"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="217"/>
        <source>Macromedia flash</source>
        <translation>Macromedia flash</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="162"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="220"/>
        <source>AVI Video</source>
        <translation>Vídeo AVI</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="195"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="241"/>
        <source>Animated PNG (APNG)</source>
        <translation>PNG Animado (APNG)</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="223"/>
        <source>RealMedia Video</source>
        <translation>Vídeo RealMedia</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="259"/>
        <source>Animated Image</source>
        <translation>Imagem Animada</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="261"/>
        <source>Image Array</source>
        <translation>Sequência de Imagens</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="167"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="226"/>
        <source>ASF Video</source>
        <translation>Vídeo ASF</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="172"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="229"/>
        <source>QuickTime Video</source>
        <translation>Vídeo QuickTime</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="232"/>
        <source>Gif Image</source>
        <translation>Imagem GIF</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="185"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="235"/>
        <source>PNG Image Array</source>
        <translation>Sequência de Imagens PNG</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="190"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="238"/>
        <source>JPEG Image Array</source>
        <translation>Sequência de Imagens JPEG</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="201"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="244"/>
        <source>SMIL</source>
        <translation>SMIL</translation>
    </message>
</context>
<context>
    <name>SelectScenes</name>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="304"/>
        <source>Select Scenes</source>
        <translation>Selecionar Cenas</translation>
    </message>
</context>
<context>
    <name>SelectionTool</name>
    <message>
        <location filename="../src/plugins/tools/selection/selectiontool.cpp" line="161"/>
        <location filename="../src/plugins/tools/selection/selectiontool.cpp" line="369"/>
        <source>Selection</source>
        <translation type="unfinished">Seleção</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/selectiontool.cpp" line="366"/>
        <source>Object Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/selectiontool.cpp" line="367"/>
        <source>O</source>
        <translation type="unfinished">O</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="81"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="85"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="92"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="72"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="92"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="87"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="88"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="91"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="95"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="102"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="82"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="102"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="97"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="98"/>
        <source>Options</source>
        <translation>Opções</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="92"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="96"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="103"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="83"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="103"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="98"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="99"/>
        <source>Select object</source>
        <translation>Selecionar objeto</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="93"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="97"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="104"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="84"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="104"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="99"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="100"/>
        <source>Set Properties</source>
        <translation>Definir Propriedades</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="133"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="137"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="144"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="124"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="144"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="139"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="140"/>
        <source>Starting at frame</source>
        <translation>Iniciar no quadro</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="141"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="145"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="152"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="152"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="147"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="148"/>
        <source>Ending at frame</source>
        <translation>Terminar no quadro</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="164"/>
        <location filename="../src/plugins/tools/color/settings.cpp" line="565"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="168"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="516"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="176"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="687"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="139"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="175"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="200"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="229"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="176"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="687"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="170"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="538"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="171"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="544"/>
        <source>Frames Total</source>
        <translation>Total de Quadros</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="174"/>
        <location filename="../src/plugins/tools/color/settings.cpp" line="190"/>
        <source>White</source>
        <translation>Branco</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="179"/>
        <source>Initial Color</source>
        <translation>Cor Inicial</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="195"/>
        <source>Ending Color</source>
        <translation>Cor Final</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="208"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="212"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="211"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="212"/>
        <source>Iterations</source>
        <translation>Iterações</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="217"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="221"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="329"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="329"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="220"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="221"/>
        <source>Loop</source>
        <translation>Repetir</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="226"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="230"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="338"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="338"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="229"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="230"/>
        <source>Loop with Reverse</source>
        <translation>Repetir com Retorno</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="273"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="277"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="375"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="178"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="375"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="279"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="279"/>
        <source>Save Tween</source>
        <translation>Salvar Intermeios</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="275"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="279"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="377"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="180"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="377"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="281"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="281"/>
        <source>Cancel Tween</source>
        <translation>Cancelar Intermeios</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="345"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="343"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="451"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="333"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="451"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="351"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="347"/>
        <source>Update Tween</source>
        <translation>Atualizar Intermeios</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="347"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="345"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="453"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="335"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="453"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="353"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="349"/>
        <source>Close Tween properties</source>
        <translation>Fechar propriedades de Intermeios</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="353"/>
        <location filename="../src/plugins/tools/color/settings.cpp" line="358"/>
        <location filename="../src/plugins/tools/color/settings.cpp" line="414"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="351"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="356"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="399"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="459"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="473"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="524"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="248"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="312"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="317"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="459"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="473"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="524"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="359"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="364"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="407"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="355"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="360"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="403"/>
        <source>Info</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="353"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="351"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="459"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="312"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="459"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="359"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="355"/>
        <source>You must select at least one object!</source>
        <translation>Você deve selecionar ao menos um objeto!</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="358"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="356"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="473"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="473"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="364"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="360"/>
        <source>You must set Tween properties first!</source>
        <translation>Você deve definir as propriedades de Intermeios primeiro!</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/settings.cpp" line="414"/>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="399"/>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="524"/>
        <location filename="../src/plugins/tools/position/settings.cpp" line="248"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="524"/>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="407"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="403"/>
        <source>Select objects for Tweening first!</source>
        <translation>Selecione objetos para o Intermeios primeiro!</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="183"/>
        <source>Initial Opacity</source>
        <translation>Opacidade Inicial</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/opacity/settings.cpp" line="199"/>
        <source>Ending Opacity</source>
        <translation>Opacidade Final</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/position/settings.cpp" line="317"/>
        <source>You must define a path for this Tween!</source>
        <translation>Você deve definir um caminho para este Intermeios!</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="185"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="185"/>
        <source>Continuous</source>
        <translation>Contínuo</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="186"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="186"/>
        <source>Partial</source>
        <translation>Parcial</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="190"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="190"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="199"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="199"/>
        <source>Speed (Degrees/Frame)</source>
        <translation>Velocidade (Graus/Quadro)</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="265"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="265"/>
        <source>Direction</source>
        <translation>Direção</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="269"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="269"/>
        <source>Clockwise</source>
        <translation>Sentido horário</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="270"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="270"/>
        <source>Counterclockwise</source>
        <translation>Sentido anti-horário</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="294"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="294"/>
        <source>Degrees Range</source>
        <translation>Graus de Rotação</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="297"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="297"/>
        <source>Start at</source>
        <translation>Iniciar em</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/papagayo/settings.cpp" line="313"/>
        <location filename="../src/plugins/tools/rotation/settings.cpp" line="313"/>
        <source>Finish at</source>
        <translation>Terminar em</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="179"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="180"/>
        <source>Width &amp; Height</source>
        <translation>Largura e Altura</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="180"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="181"/>
        <source>Only Width</source>
        <translation>Somente Largura</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="181"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="182"/>
        <source>Only Height</source>
        <translation>Somente Altura</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="182"/>
        <source>Scale in</source>
        <translation>Escalar em</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scale/settings.cpp" line="198"/>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="199"/>
        <source>Scaling Factor</source>
        <translation>Fator de Escala</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/shear/settings.cpp" line="183"/>
        <source>Shear in</source>
        <translation>Cortar em</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/pencil/settings.cpp" line="53"/>
        <source>Smoothness</source>
        <translation>Suavidade</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="59"/>
        <source>Flips</source>
        <translation>Transformações</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="70"/>
        <source>Horizontal Flip</source>
        <translation>Virar Horizontalmente</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="73"/>
        <source>Vertical Flip</source>
        <translation>Virar Verticalmente</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="76"/>
        <source>Crossed Flip</source>
        <translation>Virar para ambas as direções</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="89"/>
        <source>Order</source>
        <translation>Ordenar</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="99"/>
        <source>Send object to back</source>
        <translation>Enviar o objeto para trás</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="102"/>
        <source>Send object to back one level</source>
        <translation>Enviar o objeto para trás um nível</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="105"/>
        <source>Send object to front</source>
        <translation>Enviar o objeto para frente</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="108"/>
        <source>Send object to front one level</source>
        <translation>Enviar o objeto para frente um nível</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="124"/>
        <source>Position</source>
        <translation>Posição</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="150"/>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="229"/>
        <source>Show Tips</source>
        <translation>Exibir Dicas</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="151"/>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="230"/>
        <source>A little help for the Selection tool</source>
        <translation>Uma pequena ajuda para a ferramenta Seleção</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="166"/>
        <source>Rotation mode</source>
        <translation>Modo de rotação</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="166"/>
        <source>Double click on any node or shortcut Alt + R</source>
        <translation>Clique duplo em qualquer nó ou atalho Alt + R</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="167"/>
        <source>Arrows</source>
        <translation>Setas</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="167"/>
        <source>Movement on selection</source>
        <translation>Movimento da seleção</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="168"/>
        <source>Shift + Arrows</source>
        <translation>Shift + Setas</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="168"/>
        <source>Slow movement on selection</source>
        <translation>Movimento lento da seleção</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="169"/>
        <source>Ctrl + Arrows</source>
        <translation>Ctrl + Setas</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="169"/>
        <source>Fast movement on selection</source>
        <translation>Movimento rápido da seleção</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="170"/>
        <source>Shift + Left Mouse Button</source>
        <translation>Shift + Botão Esquerdo do Mouse</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="170"/>
        <source>Proportional scaling on selection</source>
        <translation>Escala proporcional da seleção</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="224"/>
        <source>Show Tools</source>
        <translation>Exibir Ferramentas</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/selection/settings.cpp" line="225"/>
        <source>Options panel for the Selection tool</source>
        <translation>Painel de opções para a ferramenta Seleção</translation>
    </message>
</context>
<context>
    <name>SpinControl</name>
    <message>
        <location filename="../src/libtupi/tupgradientcreator.h" line="221"/>
        <source>Radius</source>
        <translation>Raio</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupgradientcreator.h" line="230"/>
        <source>Angle</source>
        <translation>Ângulo</translation>
    </message>
</context>
<context>
    <name>StepsViewer</name>
    <message>
        <location filename="../src/plugins/tools/common/stepsviewer.cpp" line="56"/>
        <source>Interval</source>
        <translation>Intervalo</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/common/stepsviewer.cpp" line="56"/>
        <source>Frames</source>
        <translation>Quadros</translation>
    </message>
</context>
<context>
    <name>TButtonBar</name>
    <message>
        <location filename="../src/framework/tgui/tbuttonbar.cpp" line="47"/>
        <source>Left button bar</source>
        <translation>Barra de botões esquerda</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tbuttonbar.cpp" line="52"/>
        <source>Right button bar</source>
        <translation>Barra de botões direita</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tbuttonbar.cpp" line="57"/>
        <source>Bottom button bar</source>
        <translation>Barra de botões inferior</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tbuttonbar.cpp" line="62"/>
        <source>Top button bar</source>
        <translation>Barra de botões superior</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tbuttonbar.cpp" line="88"/>
        <source>Only icons</source>
        <translation>Apenas ícones</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tbuttonbar.cpp" line="91"/>
        <source>Only texts</source>
        <translation>Apenas texto</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tbuttonbar.cpp" line="96"/>
        <source>Exclusive space</source>
        <translation>Espaço exclusivo</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tbuttonbar.cpp" line="103"/>
        <source>Auto hide</source>
        <translation>Ocultar automaticamente</translation>
    </message>
</context>
<context>
    <name>TCommandHistory</name>
    <message>
        <location filename="../src/framework/tgui/tcommandhistory.cpp" line="41"/>
        <source>Undo</source>
        <translation>Desfazer</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tcommandhistory.cpp" line="42"/>
        <source>Redo</source>
        <translation>Refazer</translation>
    </message>
</context>
<context>
    <name>TFontChooser</name>
    <message>
        <location filename="../src/framework/tgui/tfontchooser.cpp" line="45"/>
        <source>Family</source>
        <translation>Família</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tfontchooser.cpp" line="49"/>
        <source>Style</source>
        <translation>Estilo</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tfontchooser.cpp" line="53"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tfontchooser.cpp" line="73"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tfontchooser.cpp" line="74"/>
        <source>Italic</source>
        <translation>Itálico</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tfontchooser.cpp" line="75"/>
        <source>Oblique</source>
        <translation>Oblíquo</translation>
    </message>
</context>
<context>
    <name>TOptionalDialog</name>
    <message>
        <location filename="../src/framework/tgui/toptionaldialog.cpp" line="56"/>
        <source>Don&apos;t show again</source>
        <translation>Não mostrar novamente</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/toptionaldialog.cpp" line="59"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/toptionaldialog.cpp" line="63"/>
        <source>Accept</source>
        <translation>Aceitar</translation>
    </message>
</context>
<context>
    <name>TRulerBase</name>
    <message>
        <location filename="../src/framework/tgui/trulerbase.cpp" line="102"/>
        <source>Change scale to 5...</source>
        <translation>Alterar escala para 5...</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/trulerbase.cpp" line="103"/>
        <source>Change scale to 10...</source>
        <translation>Alterar escala para 10...</translation>
    </message>
</context>
<context>
    <name>TViewButton</name>
    <message>
        <location filename="../src/framework/tgui/tviewbutton.cpp" line="265"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tviewbutton.cpp" line="266"/>
        <source>Only icon</source>
        <translation>Apenas ícone</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tviewbutton.cpp" line="267"/>
        <source>Only text</source>
        <translation>Apenas texto</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tviewbutton.cpp" line="270"/>
        <source>Mouse sensibility</source>
        <translation>Sensibilidade do mouse</translation>
    </message>
</context>
<context>
    <name>TWizard</name>
    <message>
        <location filename="../src/framework/tgui/twizard.cpp" line="40"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/twizard.cpp" line="41"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt; &amp;Voltar</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/twizard.cpp" line="42"/>
        <source>Next &gt;</source>
        <translation>Próximo &gt;</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/twizard.cpp" line="43"/>
        <source>&amp;Finish</source>
        <translation>&amp;Terminar</translation>
    </message>
</context>
<context>
    <name>TabDialog</name>
    <message>
        <location filename="../src/framework/tgui/tabdialog.cpp" line="89"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tabdialog.cpp" line="96"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tabdialog.cpp" line="103"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/tabdialog.cpp" line="110"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>TextConfigurator</name>
    <message>
        <location filename="../src/plugins/tools/text/textconfigurator.cpp" line="49"/>
        <source>Html</source>
        <translation>Html</translation>
    </message>
</context>
<context>
    <name>TextTool</name>
    <message>
        <location filename="../src/plugins/tools/text/texttool.cpp" line="57"/>
        <location filename="../src/plugins/tools/text/texttool.cpp" line="156"/>
        <location filename="../src/plugins/tools/text/texttool.cpp" line="160"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/text/texttool.cpp" line="157"/>
        <source>T</source>
        <translation>T</translation>
    </message>
</context>
<context>
    <name>TreeWidgetSearchLine</name>
    <message>
        <location filename="../src/framework/tgui/treewidgetsearchline.cpp" line="63"/>
        <location filename="../src/framework/tgui/treewidgetsearchline.cpp" line="78"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/treewidgetsearchline.cpp" line="266"/>
        <source>Search Columns</source>
        <translation>Buscar Colunas</translation>
    </message>
    <message>
        <location filename="../src/framework/tgui/treewidgetsearchline.cpp" line="268"/>
        <source>All Visible Columns</source>
        <translation>Todas as Colunas Visíveis</translation>
    </message>
</context>
<context>
    <name>TreeWidgetSearchLineWidget</name>
    <message>
        <location filename="../src/framework/tgui/treewidgetsearchline.cpp" line="564"/>
        <source>Clear</source>
        <translation>Limpar</translation>
    </message>
</context>
<context>
    <name>TupAbout</name>
    <message>
        <location filename="../src/libui/tupabout.cpp" line="152"/>
        <source>About</source>
        <translation>Sobre</translation>
    </message>
    <message>
        <location filename="../src/libui/tupabout.cpp" line="115"/>
        <source>Credits</source>
        <translation>Créditos</translation>
    </message>
    <message>
        <location filename="../src/libui/tupabout.cpp" line="48"/>
        <source>About Tupi</source>
        <translation>Sobre o Tupi</translation>
    </message>
    <message>
        <location filename="../src/libui/tupabout.cpp" line="136"/>
        <source>Thanks</source>
        <translation>Agradecimentos</translation>
    </message>
    <message>
        <location filename="../src/libui/tupabout.cpp" line="167"/>
        <source>License Agreement</source>
        <translation>Acordo de Licença</translation>
    </message>
    <message>
        <location filename="../src/libui/tupabout.cpp" line="168"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
</context>
<context>
    <name>TupBasicCameraInterface</name>
    <message>
        <location filename="../src/components/paintarea/tupbasiccamerainterface.cpp" line="61"/>
        <source>Tupi Camera Manager</source>
        <translation>Gerenciador de Câmera do Tupi</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupbasiccamerainterface.cpp" line="61"/>
        <source>Current resolution:</source>
        <translation>Resolução atual:</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupbasiccamerainterface.cpp" line="122"/>
        <source>Cameras</source>
        <translation>Câmeras</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupbasiccamerainterface.cpp" line="124"/>
        <source>Camera</source>
        <translation>Câmera</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupbasiccamerainterface.cpp" line="130"/>
        <source>Take picture</source>
        <translation>Tirar uma foto</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupbasiccamerainterface.cpp" line="147"/>
        <source>Close manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupbasiccamerainterface.cpp" line="219"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupbasiccamerainterface.cpp" line="219"/>
        <source>Can&apos;t create pictures directory</source>
        <translation>Não é possível criar o diretório de fotos</translation>
    </message>
</context>
<context>
    <name>TupCameraBar</name>
    <message>
        <location filename="../src/components/animation/tupcamerabar.cpp" line="59"/>
        <source>Rewind</source>
        <translation>Rebobinar</translation>
    </message>
    <message>
        <location filename="../src/components/animation/tupcamerabar.cpp" line="64"/>
        <source>Play in reverse</source>
        <translation>Reproduzir em reverso</translation>
    </message>
    <message>
        <location filename="../src/components/animation/tupcamerabar.cpp" line="69"/>
        <source>Play</source>
        <translation>Reproduzir</translation>
    </message>
    <message>
        <location filename="../src/components/animation/tupcamerabar.cpp" line="74"/>
        <source>Stop</source>
        <translation>Parar</translation>
    </message>
    <message>
        <location filename="../src/components/animation/tupcamerabar.cpp" line="79"/>
        <source>Forward</source>
        <translation>Avançar</translation>
    </message>
</context>
<context>
    <name>TupCameraDialog</name>
    <message>
        <location filename="../src/components/paintarea/tupcameradialog.cpp" line="56"/>
        <source>Camera Settings</source>
        <translation>Definições da Câmera</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcameradialog.cpp" line="72"/>
        <source>Available Camera Devices:</source>
        <translation>Dispositivos de Câmera Disponíveis:</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcameradialog.cpp" line="77"/>
        <source>Camera Detected:</source>
        <translation>Câmera Detectada:</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcameradialog.cpp" line="89"/>
        <source>Available Camera Resolutions:</source>
        <translation>Resoluções de Câmera Disponíveis:</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcameradialog.cpp" line="101"/>
        <source>Resize my project to fit camera resolution</source>
        <translation>Redimensionar meu projeto para caber na resolução da câmera</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcameradialog.cpp" line="104"/>
        <source>Use the basic camera interface (low resources)</source>
        <translation>Usar a interface básica de câmera (poucos recursos)</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcameradialog.cpp" line="110"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcameradialog.cpp" line="114"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>TupCameraInterface</name>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="63"/>
        <source>Tupi Camera Manager</source>
        <translation>Gerenciador de Câmera do Tupi</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="63"/>
        <source>Current resolution:</source>
        <translation>Resolução atual:</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="125"/>
        <source>Cameras</source>
        <translation>Câmeras</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="127"/>
        <source>Camera</source>
        <translation>Câmera</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="133"/>
        <source>Take picture</source>
        <translation>Tirar foto</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="138"/>
        <source>Show safe area</source>
        <translation>Mostrar área segura</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="139"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="145"/>
        <source>Show grid</source>
        <translation>Mostrar grade</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="146"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="156"/>
        <source>Grid spacing</source>
        <translation>Espaçamento da grade</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="167"/>
        <source>Grid color</source>
        <translation>Cor da grade</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="183"/>
        <source>Show previous images</source>
        <translation>Exibir imagens anteriores</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="184"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="194"/>
        <source>Image opacity level</source>
        <translation>Nível de opacidade da imagem</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="204"/>
        <source>Amount of images to show</source>
        <translation>Quantidade de imagens para exibir</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="231"/>
        <source>Close manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="302"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerainterface.cpp" line="302"/>
        <source>Can&apos;t create pictures directory</source>
        <translation>Não é possível criar o diretório de fotos</translation>
    </message>
</context>
<context>
    <name>TupCameraStatus</name>
    <message>
        <location filename="../src/components/animation/tupcamerastatus.cpp" line="71"/>
        <source>Scene name</source>
        <translation>Nome da cena</translation>
    </message>
    <message>
        <location filename="../src/components/animation/tupcamerastatus.cpp" line="84"/>
        <source>Frames total</source>
        <translation>Total de frames</translation>
    </message>
    <message>
        <location filename="../src/components/animation/tupcamerastatus.cpp" line="95"/>
        <source>FPS</source>
        <translation>FPS</translation>
    </message>
    <message>
        <location filename="../src/components/animation/tupcamerastatus.cpp" line="112"/>
        <source>Loop</source>
        <translation>Repetir</translation>
    </message>
    <message>
        <location filename="../src/components/animation/tupcamerastatus.cpp" line="116"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../src/components/animation/tupcamerastatus.cpp" line="129"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../src/components/animation/tupcamerastatus.cpp" line="139"/>
        <source>Post</source>
        <translation>Publicar</translation>
    </message>
</context>
<context>
    <name>TupCameraWidget</name>
    <message>
        <location filename="../src/components/animation/tupcamerawidget.cpp" line="91"/>
        <source>Scene Preview</source>
        <translation>Pré-visualização da Cena</translation>
    </message>
    <message>
        <location filename="../src/components/animation/tupcamerawidget.cpp" line="159"/>
        <source>Scale</source>
        <translation>Escala</translation>
    </message>
    <message>
        <location filename="../src/components/animation/tupcamerawidget.cpp" line="182"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
</context>
<context>
    <name>TupCameraWindow</name>
    <message>
        <location filename="../src/components/paintarea/tupcamerawindow.cpp" line="95"/>
        <source>General Camera error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerawindow.cpp" line="100"/>
        <source>Camera invalid request error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerawindow.cpp" line="105"/>
        <source>Camera service missing error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcamerawindow.cpp" line="110"/>
        <source>Camera not supported error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TupCanvas</name>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="71"/>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="78"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="79"/>
        <source>Tupi: Open 2D Magic</source>
        <translation>Tupí: Open 2D Magic</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="101"/>
        <source>Sketch Tools</source>
        <translation>Ferramentas de Rascunho</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="105"/>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="117"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="118"/>
        <source>Images</source>
        <translation>Imagens</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="109"/>
        <source>Selection Tools</source>
        <translation>Ferramentas de Seleção</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="113"/>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="145"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="146"/>
        <source>Undo</source>
        <translation>Desfazer</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="117"/>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="149"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="150"/>
        <source>Redo</source>
        <translation>Refazer</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="121"/>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="129"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="130"/>
        <source>Delete Selection</source>
        <translation>Eliminar Seleção</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="125"/>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="133"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="134"/>
        <source>Zoom In</source>
        <translation>Aproximar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="129"/>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="137"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="138"/>
        <source>Zoom Out</source>
        <translation>Afastar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="133"/>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="141"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="142"/>
        <source>Shift</source>
        <translation>Mão</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="137"/>
        <source>Pen Properties</source>
        <translation>Propriedades da Caneta</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="141"/>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="161"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="162"/>
        <source>Exposure Sheet</source>
        <translation>Tábua de Exposição</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="425"/>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="293"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="295"/>
        <source>Import a SVG file...</source>
        <translation>Importar um arquivo SVG...</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="426"/>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="294"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="296"/>
        <source>Vector</source>
        <translation>Vetor</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="463"/>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="331"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="333"/>
        <source>Information</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="465"/>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="333"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="335"/>
        <source>Image is bigger than workspace.</source>
        <translation>A imagem é maior que a área de trabalho.</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="466"/>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="334"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="336"/>
        <source>Do you want to resize it?</source>
        <translation>Deseja redimensioná-la?</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="609"/>
        <source>Scene %1</source>
        <translation>Cena %1</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="612"/>
        <source>Layer 1</source>
        <translation>Camada 1</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="615"/>
        <source>Frame 1</source>
        <translation>Quadro 1</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="628"/>
        <source>Layer %1</source>
        <translation>Camada %1</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="635"/>
        <location filename="../src/components/paintarea/tupcanvas.cpp" line="647"/>
        <source>Frame %1</source>
        <translation>Quadro %1</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="95"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="96"/>
        <source>Pencil</source>
        <translation>Lápis</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="99"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="100"/>
        <source>Ink</source>
        <translation>Tinta</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="109"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="110"/>
        <source>Ellipse</source>
        <translation>Elípse</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="113"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="114"/>
        <source>Rectangle</source>
        <translation>Retângulo</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="121"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="122"/>
        <source>Object Selection</source>
        <translation>Seleção de Objeto</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="125"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="126"/>
        <source>Nodes Selection</source>
        <translation>Seleção de Linha</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="153"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="154"/>
        <source>Color Palette</source>
        <translation>Paleta de Cor</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupcanvas.pc.cpp" line="157"/>
        <location filename="../src/components/paintarea/tupcanvas.tablet.cpp" line="158"/>
        <source>Pen Size</source>
        <translation>Tamanho da Caneta</translation>
    </message>
</context>
<context>
    <name>TupChat</name>
    <message>
        <location filename="../src/net/tupchat.cpp" line="75"/>
        <source>Send</source>
        <translation>Enviar</translation>
    </message>
    <message>
        <location filename="../src/net/tupchat.cpp" line="164"/>
        <source>Error:</source>
        <translation>Erro:</translation>
    </message>
    <message>
        <location filename="../src/net/tupchat.cpp" line="165"/>
        <source>Invalid Message. It won&apos;t be sent. Please, don&apos;t use HTML tags</source>
        <translation>Mensagem Inválida. Ela não será enviada. Por favor, não utilize etiquetas HTML</translation>
    </message>
</context>
<context>
    <name>TupColorPalette</name>
    <message>
        <location filename="../src/components/colorpalette/tupcolorpalette.cpp" line="77"/>
        <source>Color Palette</source>
        <translation>Paleta de Cor</translation>
    </message>
    <message>
        <location filename="../src/components/colorpalette/tupcolorpalette.cpp" line="127"/>
        <source>Contour</source>
        <translation>Contorno</translation>
    </message>
    <message>
        <location filename="../src/components/colorpalette/tupcolorpalette.cpp" line="128"/>
        <source>Fill</source>
        <translation>Preenchimento</translation>
    </message>
    <message>
        <location filename="../src/components/colorpalette/tupcolorpalette.cpp" line="207"/>
        <source>Color Mixer</source>
        <translation>Misturador de Cores</translation>
    </message>
    <message>
        <location filename="../src/components/colorpalette/tupcolorpalette.cpp" line="215"/>
        <source>Gradients</source>
        <translation>Gradientes</translation>
    </message>
</context>
<context>
    <name>TupColorValue</name>
    <message>
        <location filename="../src/components/colorpalette/tupcolorvalue.cpp" line="180"/>
        <source>Percent</source>
        <translation>Porcentagem</translation>
    </message>
</context>
<context>
    <name>TupConfigurationArea</name>
    <message>
        <location filename="../src/components/paintarea/tupconfigurationarea.cpp" line="296"/>
        <source>Cursor here for expand</source>
        <translation>Cursos aqui para expandir</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupconfigurationarea.cpp" line="339"/>
        <source>Properties</source>
        <translation>Propriedades</translation>
    </message>
</context>
<context>
    <name>TupConnectDialog</name>
    <message>
        <location filename="../src/net/tupconnectdialog.cpp" line="60"/>
        <source>Connection Dialog</source>
        <translation>Diálogo de Conexão</translation>
    </message>
    <message>
        <location filename="../src/net/tupconnectdialog.cpp" line="70"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="../src/net/tupconnectdialog.cpp" line="70"/>
        <source>Password</source>
        <translation>Senha</translation>
    </message>
    <message>
        <location filename="../src/net/tupconnectdialog.cpp" line="70"/>
        <source>Server</source>
        <translation>Servidor</translation>
    </message>
    <message>
        <location filename="../src/net/tupconnectdialog.cpp" line="70"/>
        <source>Port</source>
        <translation>Porta</translation>
    </message>
    <message>
        <location filename="../src/net/tupconnectdialog.cpp" line="72"/>
        <source>Store password</source>
        <translation>Armazenar senha</translation>
    </message>
    <message>
        <location filename="../src/net/tupconnectdialog.cpp" line="158"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/net/tupconnectdialog.cpp" line="158"/>
        <source>Please, fill in your password</source>
        <translation>Por favor, preencha sua senha</translation>
    </message>
</context>
<context>
    <name>TupCrashWidget</name>
    <message>
        <location filename="../src/shell/tupcrashwidget.cpp" line="112"/>
        <source>What&apos;s happening?</source>
        <translation>O que está acontecendo?</translation>
    </message>
    <message>
        <location filename="../src/shell/tupcrashwidget.cpp" line="140"/>
        <source>Executable information</source>
        <translation>Informação sobre o executável</translation>
    </message>
    <message>
        <location filename="../src/shell/tupcrashwidget.cpp" line="146"/>
        <location filename="../src/shell/tupcrashwidget.cpp" line="153"/>
        <source>Backtrace</source>
        <translation>Rastros do Problema</translation>
    </message>
</context>
<context>
    <name>TupDebugWidget</name>
    <message>
        <location filename="../src/components/debug/tupdebugwidget.cpp" line="44"/>
        <source>Debug Console</source>
        <translation>Console de Depuração</translation>
    </message>
</context>
<context>
    <name>TupDocumentView</name>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="344"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="617"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="931"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="973"/>
        <source>Object Selection</source>
        <translation>Seleção de Objetos</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="344"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="973"/>
        <source>Nodes Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="362"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="362"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="365"/>
        <source>Paste</source>
        <translation>Colar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="365"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="368"/>
        <source>Cut</source>
        <translation>Recortar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="368"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="371"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="374"/>
        <source>&amp;Group</source>
        <translation>&amp;Agrupar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="374"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="378"/>
        <source>&amp;Ungroup</source>
        <translation>&amp;Desagrupar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="379"/>
        <source>Ctrl+Shift+G</source>
        <translation>Ctrl+Shift+G</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="383"/>
        <source>Onion Skin</source>
        <translation>Papel Vegetal</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="383"/>
        <source>Ctrl+Shift+O</source>
        <translation>Ctrl+Shift+O</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="386"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1123"/>
        <source>Onion Skin Factor</source>
        <translation>Fator de Transparência</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="386"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="400"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="389"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="396"/>
        <source>@</source>
        <translation>@</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="389"/>
        <source>Export Current Frame As Image</source>
        <translation>Exportar Quadro Atual Como Imagem</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="396"/>
        <source>Export Current Frame To Gallery</source>
        <translation>Exportar Quadro Atual para a Galeria</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="400"/>
        <source>Storyboard Settings</source>
        <translation>Propriedades do Storyboard</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="403"/>
        <source>Camera</source>
        <translation>Câmera</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="403"/>
        <source>Ctrl+Shift+C</source>
        <translation>Ctrl+Shift+C</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="409"/>
        <source>Draw tools</source>
        <translation>Ferramentas de desenho</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="416"/>
        <source>Brushes</source>
        <translation>Pincéis</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="423"/>
        <source>Selection</source>
        <translation>Seleção</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="430"/>
        <source>Fill</source>
        <translation>Preenchimento</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1879"/>
        <source>Papagayo file has been imported successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="444"/>
        <source>Tweening</source>
        <translation>Intermeios</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="467"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="478"/>
        <source>Image Array</source>
        <translation>Sequência de Imagens</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="532"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="896"/>
        <source>Pencil</source>
        <translation>Lápis</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="538"/>
        <source>Ink</source>
        <translation>Tinta</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="542"/>
        <source>Eraser</source>
        <translation>Borracha</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="344"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="547"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="900"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="973"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1648"/>
        <source>PolyLine</source>
        <translation>Polilinha</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="554"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="901"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1651"/>
        <source>Line</source>
        <translation>Linha</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="561"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="900"/>
        <source>Rectangle</source>
        <translation>Retângulo</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="564"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="901"/>
        <source>Ellipse</source>
        <translation>Elípse</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="567"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="898"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="575"/>
        <source>Position Tween</source>
        <translation>Intermeio de Posição</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="580"/>
        <source>Rotation Tween</source>
        <translation>Intermeio de Rotação</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="584"/>
        <source>Scale Tween</source>
        <translation>Intermeio de Escala</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="589"/>
        <source>Shear Tween</source>
        <translation>Intermeio de Corte</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="594"/>
        <source>Opacity Tween</source>
        <translation>Intermeio de Opacidade</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="599"/>
        <source>Coloring Tween</source>
        <translation>Intermeio de Cor</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="604"/>
        <source>Compound Tween</source>
        <translation>Intermeio Composto</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="624"/>
        <source>Internal fill</source>
        <translation>Preenchimento interno</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="943"/>
        <source>Zoom In</source>
        <translation>Aproximar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="816"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="871"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="943"/>
        <source>Zoom Out</source>
        <translation>Afastar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="437"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="631"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="946"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1441"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1496"/>
        <source>Shift</source>
        <translation>Mão</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="609"/>
        <source>Papagayo Lip-sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1060"/>
        <source>Paint area actions</source>
        <translation>Ações da área de desenho</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1063"/>
        <source>Dynamic Background Properties</source>
        <translation>Propriedades do Fundo Dinâmico</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1068"/>
        <source>Frames Mode</source>
        <translation>Edição de Quadros</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1069"/>
        <source>Static BG Mode</source>
        <translation>Fundo Estático</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1070"/>
        <source>Dynamic BG Mode</source>
        <translation>Fundo Dinâmico</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1095"/>
        <source>Previous Frames</source>
        <translation>Quadros Anteriores</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1107"/>
        <source>Next Frames</source>
        <translation>Próximos Quadros</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1142"/>
        <source>Direction</source>
        <translation>Direção</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1145"/>
        <source>Left to Right</source>
        <translation>Da Esquerda para a Direita</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1146"/>
        <source>Right to Left</source>
        <translation>Da Direita para a Esquerda</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1147"/>
        <source>Top to Bottom</source>
        <translation>De Cima para Baixo</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1148"/>
        <source>Bottom to Top</source>
        <translation>De Baixo para Cima</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1151"/>
        <source>Shift Length</source>
        <translation>Comprimento do Deslocamento</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1519"/>
        <source>Export Frame As</source>
        <translation>Exportar Quadro Como</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1520"/>
        <source>Images</source>
        <translation>Imagens</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1525"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1879"/>
        <source>Information</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1525"/>
        <source>Frame has been exported successfully</source>
        <translation>O quadro foi exportado com sucesso</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1527"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1734"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1527"/>
        <source>Can&apos;t export frame as image</source>
        <translation>Não é possível exportar o quadro como imagem</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1734"/>
        <source>No cameras detected</source>
        <translation>Nenhuma câmera detectada</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1797"/>
        <location filename="../src/components/paintarea/tupdocumentview.cpp" line="1870"/>
        <source>Frame %1</source>
        <translation>Quadro %1</translation>
    </message>
</context>
<context>
    <name>TupExportWidget</name>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="1030"/>
        <source>Export to Video</source>
        <translation>Exportar para Vídeo</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="1041"/>
        <source>Export to Video File</source>
        <translation>Exportar para Arquivo de Vídeo</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="1044"/>
        <source>Export to Images Array</source>
        <translation>Exportar como Sequência de Imagens</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="1047"/>
        <source>Export to Animated Image</source>
        <translation>Exportar como Imagem Animada</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="1063"/>
        <source>Post Animation in Tupitube</source>
        <translation>Publicar Animação no Tupitube</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="1097"/>
        <source>Video Formats</source>
        <translation>Formatos de Vídeo</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="1099"/>
        <source>Open Video Format</source>
        <translation>Formato de Vídeo Aberto</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="1101"/>
        <source>Image Array</source>
        <translation>Sequência de Imagens</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="1103"/>
        <source>Animated Image</source>
        <translation>Imagem Animada</translation>
    </message>
</context>
<context>
    <name>TupExportWizard</name>
    <message>
        <location filename="../src/components/export/tupexportwizard.cpp" line="54"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwizard.cpp" line="55"/>
        <source>Back</source>
        <translation>Voltar</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwizard.cpp" line="56"/>
        <location filename="../src/components/export/tupexportwizard.cpp" line="149"/>
        <source>Next</source>
        <translation>Próxima</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwizard.cpp" line="179"/>
        <location filename="../src/components/export/tupexportwizard.cpp" line="206"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwizard.cpp" line="208"/>
        <source>Post</source>
        <translation>Publicar</translation>
    </message>
</context>
<context>
    <name>TupExposureDialog</name>
    <message>
        <location filename="../src/components/paintarea/tupexposuredialog.cpp" line="57"/>
        <source>Exposure Sheet</source>
        <translation>Tabela de Exposição</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupexposuredialog.cpp" line="114"/>
        <location filename="../src/components/paintarea/tupexposuredialog.cpp" line="131"/>
        <location filename="../src/components/paintarea/tupexposuredialog.cpp" line="134"/>
        <location filename="../src/components/paintarea/tupexposuredialog.cpp" line="264"/>
        <location filename="../src/components/paintarea/tupexposuredialog.cpp" line="279"/>
        <source>Scene</source>
        <translation>Cena</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupexposuredialog.cpp" line="144"/>
        <source>Work Team</source>
        <translation>Equipe de Trabalho</translation>
    </message>
</context>
<context>
    <name>TupExposureScene</name>
    <message>
        <location filename="../src/components/paintarea/tupexposurescene.cpp" line="65"/>
        <location filename="../src/components/paintarea/tupexposurescene.cpp" line="110"/>
        <source>Layer</source>
        <translation>Camada</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupexposurescene.cpp" line="72"/>
        <location filename="../src/components/paintarea/tupexposurescene.cpp" line="118"/>
        <location filename="../src/components/paintarea/tupexposurescene.cpp" line="154"/>
        <source>Frame</source>
        <translation>Quadro</translation>
    </message>
</context>
<context>
    <name>TupExposureSheet</name>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="60"/>
        <source>Exposure Sheet</source>
        <translation>Tabela de Exposição</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="96"/>
        <source>actions</source>
        <translation>ações</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="100"/>
        <source>Insert</source>
        <translation>Inserir</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="102"/>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="160"/>
        <source>1 frame</source>
        <translation>1 quadro</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="107"/>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="161"/>
        <source>5 frames</source>
        <translation>5 quadros</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="112"/>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="162"/>
        <source>10 frames</source>
        <translation>10 quadros</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="117"/>
        <source>20 frames</source>
        <translation>20 quadros</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="122"/>
        <source>50 frames</source>
        <translation>50 quadros</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="127"/>
        <source>100 frames</source>
        <translation>100 quadros</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="134"/>
        <source>Remove frame</source>
        <translation>Remover quadro</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="139"/>
        <source>Clear frame</source>
        <translation>Limpar quadro</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="144"/>
        <source>Lock/Unlock frame</source>
        <translation>Bloquear/Desbloquear quadro</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="149"/>
        <source>Copy frame</source>
        <translation>Copiar quadro</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="154"/>
        <source>Paste in frame</source>
        <translation>Colar no quadro</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="159"/>
        <source>Expand</source>
        <translation>Expandir</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="168"/>
        <source>Copy TL forward</source>
        <translation>Copiar LT adiante</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="169"/>
        <source>1 time</source>
        <translation>1 vez</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="170"/>
        <source>2 times</source>
        <translation>2 vezes</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="171"/>
        <source>3 times</source>
        <translation>3 vezes</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="172"/>
        <source>4 times</source>
        <translation>4 vezes</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="173"/>
        <source>5 times</source>
        <translation>5 vezes</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="247"/>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="671"/>
        <source>Layer %1</source>
        <translation>Camada %1</translation>
    </message>
    <message>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="254"/>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="501"/>
        <location filename="../src/components/exposure/tupexposuresheet.cpp" line="674"/>
        <source>Frame %1</source>
        <translation>Quadro %1</translation>
    </message>
</context>
<context>
    <name>TupGradientCreator</name>
    <message>
        <location filename="../src/libtupi/tupgradientcreator.cpp" line="77"/>
        <source>Linear</source>
        <translation>Linear</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupgradientcreator.cpp" line="77"/>
        <source>Radial</source>
        <translation>Radial</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupgradientcreator.cpp" line="77"/>
        <source>Conical</source>
        <translation>Cônico</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupgradientcreator.cpp" line="84"/>
        <source>Pad</source>
        <translation>Uniforme</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupgradientcreator.cpp" line="84"/>
        <source>Reflect</source>
        <translation>Reflexo</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupgradientcreator.cpp" line="84"/>
        <source>Repeat</source>
        <translation>Repetir</translation>
    </message>
</context>
<context>
    <name>TupGraphicsScene</name>
    <message>
        <location filename="../src/libbase/tupgraphicsscene.cpp" line="155"/>
        <source>PolyLine</source>
        <translation>Polilinha</translation>
    </message>
    <message>
        <location filename="../src/libbase/tupgraphicsscene.cpp" line="583"/>
        <location filename="../src/libbase/tupgraphicsscene.cpp" line="771"/>
        <source>/Step: 0</source>
        <translation>/Passo: 0</translation>
    </message>
    <message>
        <location filename="../src/libbase/tupgraphicsscene.cpp" line="653"/>
        <location filename="../src/libbase/tupgraphicsscene.cpp" line="803"/>
        <source>/Step: </source>
        <translation>/Passo: </translation>
    </message>
    <message>
        <location filename="../src/libbase/tupgraphicsscene.cpp" line="1212"/>
        <source>Line</source>
        <translation>Linha</translation>
    </message>
    <message>
        <location filename="../src/libbase/tupgraphicsscene.cpp" line="1297"/>
        <source>Zoom In</source>
        <translation>Aproximar</translation>
    </message>
    <message>
        <location filename="../src/libbase/tupgraphicsscene.cpp" line="1297"/>
        <source>Zoom Out</source>
        <translation>Afastar</translation>
    </message>
</context>
<context>
    <name>TupHelpBrowser</name>
    <message>
        <location filename="../src/components/help/tuphelpbrowser.cpp" line="42"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
</context>
<context>
    <name>TupHelpWidget</name>
    <message>
        <location filename="../src/components/help/tuphelpwidget.cpp" line="40"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
</context>
<context>
    <name>TupImageDialog</name>
    <message>
        <location filename="../src/components/paintarea/tupimagedialog.cpp" line="48"/>
        <source>Image Properties</source>
        <translation>Propriedades da Imagem</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupimagedialog.cpp" line="53"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupimagedialog.cpp" line="54"/>
        <source>My Picture</source>
        <translation>Minha Imagem</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupimagedialog.cpp" line="60"/>
        <source>Topics</source>
        <translation>Tópicos</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupimagedialog.cpp" line="61"/>
        <source>#topic1 #topic2 #topic3</source>
        <translation>#tópico1 #tópico2 #tópico3</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupimagedialog.cpp" line="66"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupimagedialog.cpp" line="72"/>
        <source>Just a little taste of my style :)</source>
        <translation>Uma pequena amostra do meu estilo :)</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupimagedialog.cpp" line="85"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupimagedialog.cpp" line="89"/>
        <source>Post Image</source>
        <translation>Publicar Imagem</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupimagedialog.cpp" line="111"/>
        <location filename="../src/components/paintarea/tupimagedialog.cpp" line="117"/>
        <location filename="../src/components/paintarea/tupimagedialog.cpp" line="128"/>
        <source>Set a title for the picture here!</source>
        <translation>Defina um título para a imagem aqui!</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupimagedialog.cpp" line="139"/>
        <source>Set some topic tags for the picture here!</source>
        <translation>Defina algumas etiquetas de tópicos para a imagem aqui!</translation>
    </message>
</context>
<context>
    <name>TupInfoWidget</name>
    <message>
        <location filename="../src/components/paintarea/tupinfowidget.cpp" line="124"/>
        <source>Currency Converter</source>
        <translation>Conversor de Moedas</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupinfowidget.cpp" line="128"/>
        <source>Currency</source>
        <translation>Moeda</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupinfowidget.cpp" line="132"/>
        <location filename="../src/components/paintarea/tupinfowidget.cpp" line="157"/>
        <location filename="../src/components/paintarea/tupinfowidget.cpp" line="211"/>
        <location filename="../src/components/paintarea/tupinfowidget.cpp" line="228"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupinfowidget.cpp" line="143"/>
        <source>Source</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupinfowidget.cpp" line="152"/>
        <source>Update data every</source>
        <translation>Atualizar dados a cada</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupinfowidget.cpp" line="155"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupinfowidget.cpp" line="155"/>
        <source>minute</source>
        <translation>minuto</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupinfowidget.cpp" line="157"/>
        <source>minutes</source>
        <translation>minutos</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupinfowidget.cpp" line="246"/>
        <source>Link file to Object</source>
        <translation>Vincular arquivo ao Objeto</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupinfowidget.cpp" line="247"/>
        <source>All files (*.*)</source>
        <translation>Todos os arquivos (*.*)</translation>
    </message>
</context>
<context>
    <name>TupItemManager</name>
    <message>
        <location filename="../src/components/library/tupitemmanager.cpp" line="70"/>
        <source>New folder %1</source>
        <translation>Nova pasta %1</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupitemmanager.cpp" line="229"/>
        <location filename="../src/components/library/tupitemmanager.cpp" line="350"/>
        <source>Options</source>
        <translation>Opções</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupitemmanager.cpp" line="242"/>
        <source>Edit with Inkscape</source>
        <translation>Editar com Inkscape</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupitemmanager.cpp" line="252"/>
        <source>Edit with Gimp</source>
        <translation>Editar com Gimp</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupitemmanager.cpp" line="262"/>
        <source>Edit with Krita</source>
        <translation>Editar com Krita</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupitemmanager.cpp" line="272"/>
        <source>Edit with MyPaint</source>
        <translation>Editar com MyPaint</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupitemmanager.cpp" line="283"/>
        <source>Clone</source>
        <translation>Clonar</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupitemmanager.cpp" line="286"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupitemmanager.cpp" line="232"/>
        <location filename="../src/components/library/tupitemmanager.cpp" line="289"/>
        <source>Rename</source>
        <translation>Renomear</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupitemmanager.cpp" line="235"/>
        <location filename="../src/components/library/tupitemmanager.cpp" line="292"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupitemmanager.cpp" line="303"/>
        <location filename="../src/components/library/tupitemmanager.cpp" line="354"/>
        <source>Create new raster item</source>
        <translation>Criar novo objeto raster</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupitemmanager.cpp" line="309"/>
        <location filename="../src/components/library/tupitemmanager.cpp" line="359"/>
        <source>Create new svg item</source>
        <translation>Criar novo objeto svg</translation>
    </message>
</context>
<context>
    <name>TupItemPreview</name>
    <message>
        <location filename="../src/libtupi/tupitempreview.cpp" line="56"/>
        <source>Library is empty :(</source>
        <translation>A biblioteca está vazia :(</translation>
    </message>
</context>
<context>
    <name>TupItemTweener</name>
    <message>
        <location filename="../src/store/tupitemtweener.cpp" line="610"/>
        <source>Position Tween</source>
        <translation>Intermeios de Posição</translation>
    </message>
    <message>
        <location filename="../src/store/tupitemtweener.cpp" line="613"/>
        <source>Rotation Tween</source>
        <translation>Intermeios de Rotação</translation>
    </message>
    <message>
        <location filename="../src/store/tupitemtweener.cpp" line="616"/>
        <source>Scale Tween</source>
        <translation>Intermeios de Escala</translation>
    </message>
    <message>
        <location filename="../src/store/tupitemtweener.cpp" line="619"/>
        <source>Shear Tween</source>
        <translation>Intermeios de Corte</translation>
    </message>
    <message>
        <location filename="../src/store/tupitemtweener.cpp" line="622"/>
        <source>Opacity Tween</source>
        <translation>Intermeios de Opacidade</translation>
    </message>
    <message>
        <location filename="../src/store/tupitemtweener.cpp" line="625"/>
        <source>Coloring Tween</source>
        <translation>Intermeios de Cor</translation>
    </message>
    <message>
        <location filename="../src/store/tupitemtweener.cpp" line="628"/>
        <source>Compound Tween</source>
        <translation>Intermeios Composto</translation>
    </message>
    <message>
        <location filename="../src/store/tupitemtweener.cpp" line="631"/>
        <source>Papagayo Lip-sync</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TupLayer</name>
    <message>
        <location filename="../src/store/tuplayer.cpp" line="56"/>
        <source>Layer</source>
        <translation>Camada</translation>
    </message>
</context>
<context>
    <name>TupLayerIndex</name>
    <message>
        <location filename="../src/components/timeline/tuplayerindex.cpp" line="169"/>
        <source>Layers</source>
        <translation>Camadas</translation>
    </message>
</context>
<context>
    <name>TupLibraryDialog</name>
    <message>
        <location filename="../src/components/paintarea/tuplibrarydialog.cpp" line="47"/>
        <source>Library Object</source>
        <translation>Objeto de Biblioteca</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuplibrarydialog.cpp" line="81"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuplibrarydialog.cpp" line="84"/>
        <source>Item %1</source>
        <translation>Elemento %1</translation>
    </message>
</context>
<context>
    <name>TupLibraryWidget</name>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="95"/>
        <source>Library</source>
        <translation>Biblioteca</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="162"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1445"/>
        <source>Image</source>
        <translation>Imagem</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="164"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1450"/>
        <source>Image Array</source>
        <translation>Sequência de Imagens</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="163"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1455"/>
        <source>Svg File</source>
        <translation>Arquivo Svg</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="165"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1460"/>
        <source>Svg Array</source>
        <translation>Sequência Svg</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="171"/>
        <source>Add an object to library</source>
        <translation>Adicionar um objeto à biblioteca</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="179"/>
        <source>Adds a folder to the object&apos;s list</source>
        <translation>Adiciona uma pasta à lista de objetos</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="186"/>
        <source>Inserts the selected object into the drawing area</source>
        <translation>Insere o objeto selecionado dentro da área de desenho</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="269"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1493"/>
        <source>Directory</source>
        <translation>Diretório</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="286"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="339"/>
        <source>No preview available</source>
        <translation>Nenhuma pré-visualização disponível</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="550"/>
        <source>Export object...</source>
        <translation>Exportar objeto...</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="579"/>
        <source>Info</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="579"/>
        <source>Item exported successfully!</source>
        <translation>Objeto exportado com sucesso!</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="653"/>
        <source>Couldn&apos;t create images directory!</source>
        <translation>Não é possível criar o diretório de imagens!</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="762"/>
        <source>Couldn&apos;t create vector directory!</source>
        <translation>Não é possível criar o diretório de vetor!</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="781"/>
        <source>Tupi library item</source>
        <translation>Objeto da biblioteca Tupi</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="537"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="836"/>
        <source>Images</source>
        <translation>Imagens</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="835"/>
        <source>Import images...</source>
        <translation>Importar imagens...</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="876"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1026"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1159"/>
        <source>Information</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="878"/>
        <source>Image is bigger than workspace.</source>
        <translation>A imagem é maior que a área de trabalho.</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="879"/>
        <source>Do you want to resize it?</source>
        <translation>Deseja redimensioná-la?</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="653"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="762"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="923"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="979"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1123"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1238"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1262"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="923"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="979"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1262"/>
        <source>Cannot open file: %1</source>
        <translation>Não é possível abrir o arquivo: %1</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="929"/>
        <source>Import SVG files...</source>
        <translation>Importar arquivos SVG...</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="930"/>
        <source>Vector</source>
        <translation>Vetor</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="986"/>
        <source>Choose the images directory...</source>
        <translation>Escolha o diretório de imagens...</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1009"/>
        <source>Image files found: %1.</source>
        <translation>Arquivos de Imagem encontrados: %1.</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1019"/>
        <source>Files are too big, so they will be resized.</source>
        <translation>Os arquivos são muito grandes, por isso serão redimensionados.</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1020"/>
        <source>Note: This task can take a while.</source>
        <translation>Nota: Esta tarefa pode tomar algum tempo.</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1029"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1162"/>
        <source>Do you want to continue?</source>
        <translation>Deseja continuar?</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1051"/>
        <source>Loading images...</source>
        <translation>Carregando imagens...</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1099"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1213"/>
        <source>Frame %1</source>
        <translation>Quadro %1</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1107"/>
        <source>Loading image #%1</source>
        <translation>Carregando imagem #%1</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1111"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1226"/>
        <source>ERROR!</source>
        <translation>ERRO!</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1111"/>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1226"/>
        <source>ERROR: Can&apos;t open file %1. Please, check file permissions and try again.</source>
        <translation>ERRO: Não é possível abrir o arquivo %1. Por favor, verifique as permissões do arquivo e tente novamente.</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1123"/>
        <source>No image files were found.&lt;br/&gt;Please, try another directory</source>
        <translation>Nenhum arquivo de imagem foi encontrado.&lt;br/&gt;Por favor, tente outro diretório</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1131"/>
        <source>Choose the SVG files directory...</source>
        <translation>Escolha o diretório dos arquivos SVG...</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1156"/>
        <source>%1 SVG files will be loaded.</source>
        <translation>%1 arquivos SVG serão carregados.</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1182"/>
        <source>Loading SVG files...</source>
        <translation>Carregando arquivos SVG...</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1221"/>
        <source>Loading SVG file #%1</source>
        <translation>Carregando arquivo SVG #%1</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1238"/>
        <source>No SVG files were found.&lt;br/&gt;Please, try another directory</source>
        <translation>Nenhum arquivo SVG foi encontrado.&lt;br/&gt;Por favor, tente outro diretório</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1244"/>
        <source>Import audio file...</source>
        <translation>Importar arquivo de áudio...</translation>
    </message>
    <message>
        <location filename="../src/components/library/tuplibrarywidget.cpp" line="1245"/>
        <source>Sound file</source>
        <translation>Arquivo de som</translation>
    </message>
</context>
<context>
    <name>TupListProjectDialog</name>
    <message>
        <location filename="../src/net/tuplistprojectdialog.cpp" line="54"/>
        <source>Projects List from Server</source>
        <translation>Lista de Projetos no Servidor</translation>
    </message>
    <message>
        <location filename="../src/net/tuplistprojectdialog.cpp" line="79"/>
        <source>My works:</source>
        <translation>Meus trabalhos:</translation>
    </message>
    <message>
        <location filename="../src/net/tuplistprojectdialog.cpp" line="80"/>
        <source>My contributions:</source>
        <translation>Minhas contribuições:</translation>
    </message>
    <message>
        <location filename="../src/net/tuplistprojectdialog.cpp" line="139"/>
        <location filename="../src/net/tuplistprojectdialog.cpp" line="141"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/net/tuplistprojectdialog.cpp" line="139"/>
        <location filename="../src/net/tuplistprojectdialog.cpp" line="141"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../src/net/tuplistprojectdialog.cpp" line="139"/>
        <location filename="../src/net/tuplistprojectdialog.cpp" line="141"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../src/net/tuplistprojectdialog.cpp" line="141"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../src/net/tuplistprojectdialog.cpp" line="116"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>TupMainWindow</name>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="216"/>
        <source>[ by %1 | net mode ]</source>
        <translation>[ por %1 | modo de rede ]</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="255"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="404"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="994"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="1165"/>
        <source>Information</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="255"/>
        <source>Opening a new document...</source>
        <translation>Abrindo um novo documento...</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="273"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="1218"/>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="277"/>
        <source>Animation</source>
        <translation>Animação</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="327"/>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="285"/>
        <source>Player</source>
        <translation>Reprodutor</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="404"/>
        <source>Project &lt;b&gt;%1&lt;/b&gt; opened!</source>
        <translation>Projeto &lt;b&gt;%1&lt;/b&gt; aberto!</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="484"/>
        <source>Question</source>
        <translation>Pergunta</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="486"/>
        <source>The document has been modified.</source>
        <translation>O documento foi modificado.</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="487"/>
        <source>Do you want to save the project?</source>
        <translation>Deseja salvar o projeto?</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="489"/>
        <source>Save</source>
        <translation>Salvar</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="490"/>
        <source>Discard</source>
        <translation>Descartar</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="491"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="729"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="820"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="1138"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="1173"/>
        <source>by</source>
        <translation>por</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="746"/>
        <source>Open Tupi project</source>
        <translation>Abrir projeto Tupi</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="954"/>
        <source>Import Gimp palettes</source>
        <translation>Importar paletas do Gimp</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="954"/>
        <source>Gimp Palette (*.gpl *.txt *.css)</source>
        <translation>Paleta do Gimp (*.gpl *.txt *.css)</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="994"/>
        <source>Gimp palette import was successful</source>
        <translation>Paleta do Gimp importada com sucesso</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="996"/>
        <source>Gimp palette import was unsuccessful</source>
        <translation>Importação da paleta do Gimp falhou</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="1092"/>
        <source>Save Project As</source>
        <translation>Salvar Projeto Como</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="1109"/>
        <source>Directory does not exist! Please, choose another path.</source>
        <translation>Diretório não existe! Por favor, escolha outro caminho.</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="1165"/>
        <source>Project &lt;b&gt;%1&lt;/b&gt; saved</source>
        <translation>Projeto &lt;b&gt;%1&lt;/b&gt; salvo</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="1428"/>
        <source>Import project package</source>
        <translation>Importar pacote do projeto</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="1437"/>
        <source>Can&apos;t import project. File is empty!</source>
        <translation>Não é possível importar o projeto. O arquivo está vazio!</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="1441"/>
        <source>Can&apos;t save the project. File doesn&apos;t exist!</source>
        <translation>Não é possível salvar o projeto. O arquivo não existe!</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="1457"/>
        <source>Fatal Error</source>
        <translation>Erro Fatal</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="1459"/>
        <source>The connection to the server has been lost.</source>
        <translation>A conexão com o servidor foi perdida.</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="1460"/>
        <source>Please, try to connect again in a while</source>
        <translation>Por favor, tente conectar novamente daqui a pouco</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="824"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="996"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="1109"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="1124"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="1175"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="1437"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="1441"/>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="691"/>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="702"/>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="713"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="824"/>
        <source>Cannot open project!</source>
        <translation>Não é possível abrir o projeto!</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="747"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="1093"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="1429"/>
        <source>Tupi Project Package (*.tup)</source>
        <translation>Pacote do Projeto Tupi (*.tup)</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="111"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="216"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="635"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="729"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="820"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="1138"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="1173"/>
        <source>Tupi: Open 2D Magic</source>
        <translation>Tupi: Magia 2D Livre</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="933"/>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="470"/>
        <source>Tip of the day</source>
        <translation>Dica do dia</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="933"/>
        <source>Show on start</source>
        <translation>Mostrar no início</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="933"/>
        <source>Previous tip</source>
        <translation>Dica anterior</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="933"/>
        <source>Next tip</source>
        <translation>Próxima dica</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="933"/>
        <location filename="../src/shell/tupmainwindow.cpp" line="1462"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="1124"/>
        <source>You have no permission to create this file. Please, choose another path.</source>
        <translation>Você não tem permissão para criar este arquivo. Por favor, escolha outro caminho.</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="1175"/>
        <source>Cannot save the project!</source>
        <translation>Não é possível salvar o projeto!</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow.cpp" line="1219"/>
        <source>New camera</source>
        <translation>Nova câmera</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="64"/>
        <source>Shift+P</source>
        <translation>Shift+P</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="75"/>
        <source>Shift+B</source>
        <translation>Shift+B</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="86"/>
        <source>Shift+L</source>
        <translation>Shift+L</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="90"/>
        <source>Bitmap</source>
        <translation>Mapa de bits</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="90"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="93"/>
        <source>Bitmap Array</source>
        <translation>Sequência de Mapa de Bits</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="93"/>
        <source>Alt+Shift+B</source>
        <translation>Alt+Shift+B</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="96"/>
        <source>SVG File</source>
        <translation>Arquivo SVG</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="96"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="99"/>
        <source>SVG Array</source>
        <translation>Sequência de SVG</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="99"/>
        <source>Alt+Shift+S</source>
        <translation>Alt+Shift+S</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="124"/>
        <source>Shift+E</source>
        <translation>Shift+E</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="139"/>
        <source>Shift+H</source>
        <translation>Shift+H</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="154"/>
        <source>Shift+T</source>
        <translation>Shift+T</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="164"/>
        <source>Shift+D</source>
        <translation>Shift+D</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="195"/>
        <source>&amp;File</source>
        <translation>&amp;Arquivo</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="204"/>
        <source>Recents</source>
        <translation>Recentes</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="225"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editar</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="252"/>
        <source>&amp;Window</source>
        <translation>&amp;Janela</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="272"/>
        <source>Modules</source>
        <translation>Módulos</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="293"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="301"/>
        <source>News</source>
        <translation>Notícias</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="314"/>
        <source>&amp;Help</source>
        <translation>&amp;Ajuda</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="361"/>
        <source>New project</source>
        <translation>Novo projeto</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="361"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="363"/>
        <source>Open new project</source>
        <translation>Abrir novo projeto</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="366"/>
        <source>Open project</source>
        <translation>Abrir projeto</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="366"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="369"/>
        <source>Load existent project</source>
        <translation>Carregar projeto existente</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="409"/>
        <source>Shift+G</source>
        <translation>Shift+G</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="115"/>
        <source>Shift+C</source>
        <translation>Shift+C</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="235"/>
        <source>&amp;Import</source>
        <translation>&amp;Importar</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="382"/>
        <source>Save project</source>
        <translation>Salvar projeto</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="383"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="385"/>
        <source>Save current project in current location</source>
        <translation>Salvar projeto atual no local atual</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="390"/>
        <source>Save project &amp;As...</source>
        <translation>Salvar projeto &amp;Como...</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="391"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="394"/>
        <source>Open dialog box to save current project in any location</source>
        <translation>Abrir caixa de diálogo para salvar o projeto atual em qualquer local</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="400"/>
        <source>Cl&amp;ose project</source>
        <translation>F&amp;echar o projeto</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="400"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="403"/>
        <source>Close active project</source>
        <translation>Fechar projeto ativo</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="408"/>
        <source>&amp;Import GIMP palettes</source>
        <translation>&amp;Importar paletas do GIMP</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="410"/>
        <source>Import palettes</source>
        <translation>Importar paletas</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="413"/>
        <source>&amp;Import Papagayo Lip-sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="414"/>
        <source>Alt+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="415"/>
        <source>Import Papagayo lip-sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="419"/>
        <source>&amp;Export Project</source>
        <translation>&amp;Exportar Projeto</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="419"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="421"/>
        <source>Export project to several video formats</source>
        <translation>Exportar projeto para vários formatos de vídeo</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="425"/>
        <source>E&amp;xit</source>
        <translation>S&amp;air</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="425"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="427"/>
        <source>Close application</source>
        <translation>Fechar aplicativo</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="451"/>
        <source>Pr&amp;eferences...</source>
        <translation>Pr&amp;eferências...</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="452"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="454"/>
        <source>Opens the preferences dialog box</source>
        <translation>Abre a caixa de diálogo de preferências</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="468"/>
        <source>About Tupi</source>
        <translation>Sobre o Tupi</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="468"/>
        <source>Ctrl+K</source>
        <translation>Ctrl+K</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="470"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="526"/>
        <source>Actions Bar</source>
        <translation>Barra de Ações</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="666"/>
        <source>Undo</source>
        <translation>Desfazer</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="669"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="672"/>
        <source>Redo</source>
        <translation>Refazer</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="675"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="684"/>
        <source>Import Papagayo project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="684"/>
        <source>Papagayo Project (*.pgo)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="691"/>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="702"/>
        <location filename="../src/shell/tupmainwindow_gui.cpp" line="713"/>
        <source>Papagayo project is invalid!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TupNetProjectManagerHandler</name>
    <message>
        <location filename="../src/net/tupnetprojectmanagerhandler.cpp" line="80"/>
        <source>Communications</source>
        <translation>Comunicações</translation>
    </message>
    <message>
        <location filename="../src/net/tupnetprojectmanagerhandler.cpp" line="471"/>
        <location filename="../src/net/tupnetprojectmanagerhandler.cpp" line="536"/>
        <source>Information</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../src/net/tupnetprojectmanagerhandler.cpp" line="248"/>
        <location filename="../src/net/tupnetprojectmanagerhandler.cpp" line="507"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/net/tupnetprojectmanagerhandler.cpp" line="84"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="../src/net/tupnetprojectmanagerhandler.cpp" line="89"/>
        <source>Notices</source>
        <translation>Notificações</translation>
    </message>
    <message>
        <location filename="../src/net/tupnetprojectmanagerhandler.cpp" line="248"/>
        <source>Unable to connect to server</source>
        <translation>Não é possível conectar ao servidor</translation>
    </message>
    <message>
        <location filename="../src/net/tupnetprojectmanagerhandler.cpp" line="319"/>
        <source>Fatal Error</source>
        <translation>Erro Fatal</translation>
    </message>
    <message>
        <location filename="../src/net/tupnetprojectmanagerhandler.cpp" line="321"/>
        <source>User &quot;%1&quot; is disabled.
Please, contact the animation server admin to get access.</source>
        <translation>Usuário &quot;%1&quot; está desabilitado.
Por favor, contatar o administrador do servidor de animação para obter acesso.</translation>
    </message>
    <message>
        <location filename="../src/net/tupnetprojectmanagerhandler.cpp" line="471"/>
        <source>User has no available projects in the server</source>
        <translation>Usuário não possui projetos disponíveis no servidor</translation>
    </message>
    <message>
        <location filename="../src/net/tupnetprojectmanagerhandler.cpp" line="505"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="../src/net/tupnetprojectmanagerhandler.cpp" line="529"/>
        <source>Notice</source>
        <translation>Notificação</translation>
    </message>
</context>
<context>
    <name>TupNewItemDialog</name>
    <message>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="66"/>
        <source>Create new raster item</source>
        <translation>Criar novo objeto raster</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="73"/>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="182"/>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="186"/>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="205"/>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="209"/>
        <source>Transparent</source>
        <translation>Transparente</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="74"/>
        <source>White</source>
        <translation>Branco</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="75"/>
        <source>Black</source>
        <translation>Preto</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="87"/>
        <source>Create new vector item</source>
        <translation>Criar novo objeto vetorial</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="123"/>
        <source>&amp;Name:</source>
        <translation>&amp;Nome:</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="124"/>
        <source>&amp;Extension:</source>
        <translation>&amp;Extensão:</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="125"/>
        <source>&amp;Width:</source>
        <translation>&amp;Largura:</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="126"/>
        <source>&amp;Height:</source>
        <translation>&amp;Altura:</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="129"/>
        <source>&amp;Background:</source>
        <translation>&amp;Fundo:</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupnewitemdialog.cpp" line="133"/>
        <source>&amp;Open it with:</source>
        <translation>&amp;Abrir com:</translation>
    </message>
</context>
<context>
    <name>TupNewProject</name>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="75"/>
        <source>Create a new project</source>
        <translation>Criar um novo projeto</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="81"/>
        <source>Project Name</source>
        <translation>Nome do Projeto</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="85"/>
        <source>my_project</source>
        <translation>meu_projeto</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="88"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="92"/>
        <source>Your name</source>
        <translation>Seu nome</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="95"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="99"/>
        <source>Just for fun!</source>
        <translation>Só por diversão!</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="103"/>
        <source>Presets</source>
        <translation>Formatos</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="106"/>
        <source>Free format</source>
        <translation>Formato livre</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="107"/>
        <source>520x380 - 24</source>
        <translation>520x380 - 24</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="108"/>
        <source>640x480 - 24</source>
        <translation>640x480 - 24</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="109"/>
        <source>480p (PAL DV/DVD) - 25</source>
        <translation>480p (PAL DV/DVD) - 25</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="110"/>
        <source>576p (PAL DV/DVD) - 25</source>
        <translation>576p (PAL DV/DVD) - 25</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="111"/>
        <source>720p (HD) - 25</source>
        <translation>720p (HD) - 25</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="112"/>
        <source>1280p (Full HD) - 25</source>
        <translation>1280p (Full HD) - 25</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="126"/>
        <source>Background</source>
        <translation>Fundo</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="127"/>
        <source>Click here to change background color</source>
        <translation>Clique aqui para alterar a cor do fundo</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="145"/>
        <source>Dimension</source>
        <translation>Dimensão</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="226"/>
        <source>Settings</source>
        <translation>Definições</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="279"/>
        <source>Please, set a name for the project</source>
        <translation>Por favor, defina um nome para o projeto</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="285"/>
        <source>Please, fill in your username</source>
        <translation>Por favor, preencha com seu nome de usuário</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="290"/>
        <source>Please, fill in your password</source>
        <translation>Por favor, preencha com sua senha</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="295"/>
        <source>Please, fill in the server name or IP</source>
        <translation>Por favor, preencha com o nome do servidor ou IP</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="325"/>
        <source>White</source>
        <translation>Branco</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="119"/>
        <source>Options</source>
        <translation>Opções</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="135"/>
        <source>FPS</source>
        <translation>FPS</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="161"/>
        <source>Tupitube project</source>
        <translation>Projeto Tupitube</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="167"/>
        <source>Project info</source>
        <translation>Informação do projeto</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="228"/>
        <source>Password</source>
        <translation>Senha</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="228"/>
        <source>Server</source>
        <translation>Servidor</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="228"/>
        <source>Port</source>
        <translation>Porta</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="228"/>
        <source>Username</source>
        <translation>Nome de usuário</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="233"/>
        <source>Store password</source>
        <translation>Armazenar senha</translation>
    </message>
    <message>
        <location filename="../src/shell/tupnewproject.cpp" line="279"/>
        <location filename="../src/shell/tupnewproject.cpp" line="285"/>
        <location filename="../src/shell/tupnewproject.cpp" line="290"/>
        <location filename="../src/shell/tupnewproject.cpp" line="295"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
</context>
<context>
    <name>TupNotice</name>
    <message>
        <location filename="../src/net/tupnotice.cpp" line="58"/>
        <source>Notices</source>
        <translation>Notificações</translation>
    </message>
</context>
<context>
    <name>TupOnionOpacityDialog</name>
    <message>
        <location filename="../src/components/paintarea/tuponionopacitydialog.cpp" line="50"/>
        <source>Onion Paper</source>
        <translation>Papel Vegetal</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuponionopacitydialog.cpp" line="103"/>
        <source>-0.05</source>
        <translation>-0.05</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuponionopacitydialog.cpp" line="107"/>
        <source>-0.01</source>
        <translation>-0.01</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuponionopacitydialog.cpp" line="119"/>
        <source>+0.01</source>
        <translation>+0.01</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuponionopacitydialog.cpp" line="123"/>
        <source>+0.05</source>
        <translation>+0.05</translation>
    </message>
</context>
<context>
    <name>TupPaintArea</name>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="80"/>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="1378"/>
        <source>Pencil</source>
        <translation>Lápis</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="161"/>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="696"/>
        <source>Nodes Selection</source>
        <translation>Seleção de Contorno</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="171"/>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="591"/>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="1364"/>
        <source>PolyLine</source>
        <translation>Polilinha</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="182"/>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="644"/>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="696"/>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="1379"/>
        <source>Object Selection</source>
        <translation>Seleção de Objetos</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="199"/>
        <source>Drawing area</source>
        <translation>Área de desenho</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="204"/>
        <source>Cut</source>
        <translation>Recortar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="204"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="205"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="205"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="206"/>
        <source>Paste</source>
        <translation>Colar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="206"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="208"/>
        <source>Paste in...</source>
        <translation>Colar em...</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="209"/>
        <source>next 5 frames</source>
        <translation>próximos 5 quadros</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="210"/>
        <source>next 10 frames</source>
        <translation>próximos 10 quadros</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="211"/>
        <source>next 20 frames</source>
        <translation>próximos 20 quadros</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="212"/>
        <source>next 50 frames</source>
        <translation>próximos 50 quadros</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="213"/>
        <source>next 100 frames</source>
        <translation>próximos 100 quadros</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="223"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="226"/>
        <source>Send</source>
        <translation>Enviar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="229"/>
        <source>To back</source>
        <translation>Para trás</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="230"/>
        <source>To front</source>
        <translation>Para frente</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="231"/>
        <source>One level to back</source>
        <translation>Para trás um nível</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="232"/>
        <source>One level to front</source>
        <translation>Para frente um nível</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="1370"/>
        <source>Rectangle</source>
        <translation>Retângulo</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="1371"/>
        <source>Ellipse</source>
        <translation>Elípse</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="176"/>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="1372"/>
        <source>Line</source>
        <translation>Linha</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="238"/>
        <source>Add to library...</source>
        <translation>Adicionar à biblioteca...</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="1085"/>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="1430"/>
        <source>Frame %1</source>
        <translation>Quadro %1</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="1185"/>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="1223"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="1185"/>
        <location filename="../src/components/paintarea/tuppaintarea.cpp" line="1223"/>
        <source>No items selected</source>
        <translation>Nenhum item selecionado</translation>
    </message>
</context>
<context>
    <name>TupPaintAreaBase</name>
    <message>
        <location filename="../src/libbase/tuppaintareabase.cpp" line="393"/>
        <source>No Scene!</source>
        <translation>Não existem Cenas!</translation>
    </message>
    <message>
        <location filename="../src/libbase/tuppaintareabase.cpp" line="400"/>
        <source>Locked!</source>
        <translation>Bloqueado!</translation>
    </message>
    <message>
        <location filename="../src/libbase/tuppaintareabase.cpp" line="469"/>
        <source>No Frames!</source>
        <translation>Não existem Quadros!</translation>
    </message>
    <message>
        <location filename="../src/libbase/tuppaintareabase.cpp" line="472"/>
        <source>No Layers!</source>
        <translation>Não existem Camadas!</translation>
    </message>
</context>
<context>
    <name>TupPaintAreaConfig</name>
    <message>
        <location filename="../src/libui/tuppaintareaconfig.cpp" line="59"/>
        <source>Grid color</source>
        <translation>Cor da grade</translation>
    </message>
    <message>
        <location filename="../src/libui/tuppaintareaconfig.cpp" line="65"/>
        <source>Grid separation</source>
        <translation>Separação da grade</translation>
    </message>
    <message>
        <location filename="../src/libui/tuppaintareaconfig.cpp" line="74"/>
        <source>Background color</source>
        <translation>Cor de fundo</translation>
    </message>
    <message>
        <location filename="../src/libui/tuppaintareaconfig.cpp" line="80"/>
        <source>Onion skin color</source>
        <translation>Cor do papel vegetal</translation>
    </message>
    <message>
        <location filename="../src/libui/tuppaintareaconfig.cpp" line="86"/>
        <source>Onion skin background </source>
        <translation>Fundo do papel vegetal</translation>
    </message>
</context>
<context>
    <name>TupPaintAreaStatus</name>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="76"/>
        <source>Action Safe Area</source>
        <translation>Área Segura de Ação</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="77"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="85"/>
        <source>Show grid</source>
        <translation>Mostrar grade</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="86"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="94"/>
        <source>Full screen</source>
        <translation>Tela cheia</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="95"/>
        <source>F11</source>
        <translation>F11</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="106"/>
        <source>Current Frame</source>
        <translation>Quadro atual</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="113"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="127"/>
        <source>Zoom</source>
        <translation>Aproximação</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="147"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="159"/>
        <source>Rotate Workspace</source>
        <translation>Rotacionar Área de Trabalho</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="191"/>
        <source>Background Color</source>
        <translation>Cor de Fundo</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="192"/>
        <source>Click here to change background color</source>
        <translation>Clique aqui para alterar a cor de fundo</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="198"/>
        <source>Brush Color</source>
        <translation>Cor do Pincel</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="199"/>
        <source>Click here to change brush color</source>
        <translation>Clique aqui para alterar a cor do pincel</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppaintareastatus.cpp" line="182"/>
        <source>Antialiasing</source>
        <translation>Antisserrilhamento</translation>
    </message>
</context>
<context>
    <name>TupPenDialog</name>
    <message>
        <location filename="../src/components/paintarea/tuppendialog.cpp" line="50"/>
        <source>Pen Size</source>
        <translation>Tamanho da Caneta</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppendialog.cpp" line="102"/>
        <source>-5</source>
        <translation>-5</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppendialog.cpp" line="106"/>
        <source>-1</source>
        <translation>-1</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppendialog.cpp" line="114"/>
        <source>+1</source>
        <translation>+1</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuppendialog.cpp" line="118"/>
        <source>+5</source>
        <translation>+5</translation>
    </message>
</context>
<context>
    <name>TupPenWidget</name>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="52"/>
        <source>Pen Properties</source>
        <translation>Propriedades da Caneta</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="63"/>
        <source>Thickness</source>
        <translation>Espessura</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="76"/>
        <source>Dashes</source>
        <translation>Padrão</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="83"/>
        <source>Solid</source>
        <translation>Sólido</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="85"/>
        <source>Dash</source>
        <translation>Traço</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="87"/>
        <source>Dot</source>
        <translation>Pontos</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="89"/>
        <source>Dash dot</source>
        <translation>Traços e pontos</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="91"/>
        <source>Dash dot dot</source>
        <translation>Traço ponto ponto</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="108"/>
        <source>Cap</source>
        <translation>Borda</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="114"/>
        <source>Flat</source>
        <translation>Achatado</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="115"/>
        <source>Square</source>
        <translation>Quadrado</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="116"/>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="132"/>
        <source>Round</source>
        <translation>Redondo</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="125"/>
        <source>Join</source>
        <translation>Junta</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="130"/>
        <source>Miter</source>
        <translation>Ponta</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="131"/>
        <source>Bevel</source>
        <translation>Ângulo reto</translation>
    </message>
    <message>
        <location filename="../src/components/pen/tuppenwidget.cpp" line="141"/>
        <source>Brush</source>
        <translation>Pincel</translation>
    </message>
</context>
<context>
    <name>TupPreferences</name>
    <message>
        <location filename="../src/libui/tuppreferences.cpp" line="202"/>
        <source>Application TupPreferences</source>
        <translation>Preferências do Tupi</translation>
    </message>
    <message>
        <location filename="../src/libui/tuppreferences.cpp" line="205"/>
        <source>General</source>
        <translation>Geral</translation>
    </message>
    <message>
        <location filename="../src/libui/tuppreferences.cpp" line="208"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../src/libui/tuppreferences.cpp" line="211"/>
        <source>Font</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <location filename="../src/libui/tuppreferences.cpp" line="214"/>
        <source>Workspace</source>
        <translation>Área de Trabalho</translation>
    </message>
</context>
<context>
    <name>TupPreferences::GeneralPage</name>
    <message>
        <location filename="../src/libui/tuppreferences.cpp" line="127"/>
        <source>Tupi Home</source>
        <translation>Diretório de Instalação</translation>
    </message>
    <message>
        <location filename="../src/libui/tuppreferences.cpp" line="127"/>
        <source>Cache</source>
        <translation>Cache</translation>
    </message>
    <message>
        <location filename="../src/libui/tuppreferences.cpp" line="127"/>
        <source>Browser</source>
        <translation>Navegador</translation>
    </message>
    <message>
        <location filename="../src/libui/tuppreferences.cpp" line="128"/>
        <source>Open last project</source>
        <translation>Último projeto aberto</translation>
    </message>
    <message>
        <location filename="../src/libui/tuppreferences.cpp" line="128"/>
        <source>Auto save (minutes)</source>
        <translation>Salvvar automaticamente (minutos)</translation>
    </message>
</context>
<context>
    <name>TupProjectActionBar</name>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="97"/>
        <source>Insert a layer</source>
        <translation>Inserir uma camada</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="98"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="109"/>
        <source>Remove the layer</source>
        <translation>Remover a camada</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="110"/>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="121"/>
        <source>Move layer up</source>
        <translation>Mover camada para cima</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="122"/>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="197"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="132"/>
        <source>Move layer down</source>
        <translation>Mover camada para baixo</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="133"/>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="208"/>
        <source>F10</source>
        <translation>F10</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="148"/>
        <source>Insert frame</source>
        <translation>Inserir quadro</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="159"/>
        <source>Remove the frame</source>
        <translation>Remover o quadro</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="177"/>
        <source>Move frame up</source>
        <translation>Mover o quadro para cima</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="178"/>
        <source>F8</source>
        <translation>F8</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="196"/>
        <source>Move frame down</source>
        <translation>Mover o quadro para baixo</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="207"/>
        <source>Lock frame</source>
        <translation>Bloquear quadro</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="218"/>
        <source>Lock layer</source>
        <translation>Bloquear camada</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="228"/>
        <source>Insert a scene</source>
        <translation>Inserir uma cena</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="238"/>
        <source>Remove the scene</source>
        <translation>Remover a cena</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="248"/>
        <source>Move scene up</source>
        <translation>Mover a cena para cima</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="258"/>
        <source>Move scene down</source>
        <translation>Mover a cena para baixo</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="268"/>
        <source>Lock scene</source>
        <translation>Bloquear a cena</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="345"/>
        <source>Do you want to remove this frame?</source>
        <translation>Deseja remover este quadro?</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="345"/>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="366"/>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="386"/>
        <source>Remove?</source>
        <translation>Remover?</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="366"/>
        <source>Do you want to remove this layer?</source>
        <translation>Deseja remover esta camada?</translation>
    </message>
    <message>
        <location filename="../src/libtupi/tupprojectactionbar.cpp" line="386"/>
        <source>Do you want to remove this scene?</source>
        <translation>Deseja remover esta cena?</translation>
    </message>
</context>
<context>
    <name>TupProjectManager</name>
    <message>
        <location filename="../src/store/tupprojectmanager.cpp" line="207"/>
        <source>Scene %1</source>
        <translation>Cena %1</translation>
    </message>
    <message>
        <location filename="../src/store/tupprojectmanager.cpp" line="210"/>
        <source>Layer %1</source>
        <translation>Camada %1</translation>
    </message>
    <message>
        <location filename="../src/store/tupprojectmanager.cpp" line="213"/>
        <source>Frame %1</source>
        <translation>Quadro %1</translation>
    </message>
</context>
<context>
    <name>TupScene</name>
    <message>
        <location filename="../src/store/tupscene.cpp" line="648"/>
        <source>Layer %1</source>
        <translation>Camada %1</translation>
    </message>
    <message>
        <location filename="../src/store/tupscene.cpp" line="649"/>
        <source>Frame %1</source>
        <translation>Quadro %1</translation>
    </message>
    <message>
        <location filename="../src/store/tupscene.cpp" line="190"/>
        <source>Sound layer %1</source>
        <translation>Camada de som %1</translation>
    </message>
</context>
<context>
    <name>TupSceneTabWidget</name>
    <message>
        <location filename="../src/components/exposure/tupscenetabwidget.cpp" line="77"/>
        <source>Layers</source>
        <translation>Camadas</translation>
    </message>
</context>
<context>
    <name>TupScenesWidget</name>
    <message>
        <location filename="../src/components/scenes/tupsceneswidget.cpp" line="57"/>
        <source>Scenes Manager</source>
        <translation>Gerenciador de Cenas</translation>
    </message>
    <message>
        <location filename="../src/components/scenes/tupsceneswidget.cpp" line="94"/>
        <source>Filter here...</source>
        <translation>Filtrar aqui...</translation>
    </message>
    <message>
        <location filename="../src/components/scenes/tupsceneswidget.cpp" line="155"/>
        <location filename="../src/components/scenes/tupsceneswidget.cpp" line="159"/>
        <location filename="../src/components/scenes/tupsceneswidget.cpp" line="190"/>
        <source>Scene %1</source>
        <translation>Cena %1</translation>
    </message>
    <message>
        <location filename="../src/components/scenes/tupsceneswidget.cpp" line="165"/>
        <source>Layer %1</source>
        <translation>Camada %1</translation>
    </message>
    <message>
        <location filename="../src/components/scenes/tupsceneswidget.cpp" line="168"/>
        <source>Frame %1</source>
        <translation>Quadro %1</translation>
    </message>
</context>
<context>
    <name>TupScreen</name>
    <message>
        <location filename="../src/components/animation/tupscreen.cpp" line="482"/>
        <source>Rendering...</source>
        <translation>Renderizando...</translation>
    </message>
</context>
<context>
    <name>TupSplash</name>
    <message>
        <location filename="../src/shell/tupsplash.cpp" line="53"/>
        <source>Version </source>
        <translation>Versão</translation>
    </message>
</context>
<context>
    <name>TupStoryBoardDialog</name>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="117"/>
        <source>Storyboard Settings</source>
        <translation>Propriedades do Storyboard</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="128"/>
        <source>&amp;PDF</source>
        <translation>&amp;PDF</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="129"/>
        <source>Export as PDF</source>
        <translation>Exportar como PDF</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="132"/>
        <source>&amp;HTML</source>
        <translation>&amp;HTML</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="133"/>
        <source>Export as HTML</source>
        <translation>Exportar como HTML</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="137"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="146"/>
        <source>&amp;Post</source>
        <translation>&amp;Publicar</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="202"/>
        <source>Storyboard General Information</source>
        <translation>Informações Gerais do Storyboard</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="206"/>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="269"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="212"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="217"/>
        <source>Summary</source>
        <translation>Sumário</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="236"/>
        <source>Topics</source>
        <translation>Tópicos</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="265"/>
        <source>Scene Information</source>
        <translation>Informação da Cena</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="274"/>
        <source>Duration</source>
        <translation>Duração</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="279"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="314"/>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="334"/>
        <source>Cover</source>
        <translation>Capa</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="328"/>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="411"/>
        <source>Storyboard</source>
        <translation>Storyboard</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="360"/>
        <source>Scene</source>
        <translation>Cena</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="374"/>
        <source>Scene No %1 - Information</source>
        <translation>Cena No %1 - Informação</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="570"/>
        <source>Choose a directory...</source>
        <translation>Escolha um diretório...</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="574"/>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="612"/>
        <source>Info</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="574"/>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="612"/>
        <source>Storyboard exported successfully!</source>
        <translation>Storyboard exportado com sucesso!</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="587"/>
        <source>Save PDF file</source>
        <translation>Salvar arquivo PDF</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="587"/>
        <source>PDF file (*.pdf)</source>
        <translation>Arquivo PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="700"/>
        <source>PDF</source>
        <translation>PDF</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tupstoryboarddialog.cpp" line="702"/>
        <source>Html</source>
        <translation>Html</translation>
    </message>
</context>
<context>
    <name>TupSymbolEditor</name>
    <message>
        <location filename="../src/components/library/tupsymboleditor.cpp" line="74"/>
        <source>Symbol editor</source>
        <translation>Editor de objetos</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupsymboleditor.cpp" line="84"/>
        <source>Brushes</source>
        <translation>Pincéis</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupsymboleditor.cpp" line="87"/>
        <source>Selection</source>
        <translation>Seleção</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupsymboleditor.cpp" line="90"/>
        <source>Fill</source>
        <translation>Preenchimento</translation>
    </message>
    <message>
        <location filename="../src/components/library/tupsymboleditor.cpp" line="93"/>
        <source>View</source>
        <translation>Visualização</translation>
    </message>
</context>
<context>
    <name>TupThemeSelector</name>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="73"/>
        <source>General</source>
        <translation>Geral</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="77"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="77"/>
        <source>Base</source>
        <translation>Base</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="77"/>
        <source>Foreground</source>
        <translation>Primeiro plano</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="77"/>
        <source>Background</source>
        <translation>Fundo</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="78"/>
        <source>Button</source>
        <translation>Botão</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="78"/>
        <source>Button Text</source>
        <translation>Texto do Botão</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="101"/>
        <source>Effects</source>
        <translation>Efeitos</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="105"/>
        <source>Light</source>
        <translation>Iluminação</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="105"/>
        <source>Midlight</source>
        <translation>Iluminação média</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="105"/>
        <source>Dark</source>
        <translation>Obscuridade</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="105"/>
        <source>Mid</source>
        <translation>Meio</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="127"/>
        <source>Selections</source>
        <translation>Seleções</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="131"/>
        <source>Highlight</source>
        <translation>Ressaltar</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="131"/>
        <source>Highlighted Text</source>
        <translation>Texto ressaltado</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="148"/>
        <source>Text effects</source>
        <translation>Efeitos de texto</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="151"/>
        <source>Bright Text</source>
        <translation>Texto Brilhante</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="151"/>
        <source>Link</source>
        <translation>Vínculo</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="151"/>
        <source>Link Visited</source>
        <translation>Vínculo Visitado</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="169"/>
        <location filename="../src/libui/tupthemeselector.cpp" line="173"/>
        <source>Schema</source>
        <translation>Esquema</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="173"/>
        <source>Owner</source>
        <translation>Proprietário</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="173"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="182"/>
        <source>Save schema</source>
        <translation>Salvar esquema</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="189"/>
        <source>Style</source>
        <translation>Estilo</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="193"/>
        <source>Use this colors</source>
        <translation>Usar estas cores</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="282"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/libui/tupthemeselector.cpp" line="282"/>
        <source>Please choose a theme name</source>
        <translation>Por favor, escolha um nome de tema</translation>
    </message>
</context>
<context>
    <name>TupTimeLine</name>
    <message>
        <location filename="../src/components/timeline/tuptimeline.cpp" line="60"/>
        <source>Time Line</source>
        <translation>Linha de Tempo</translation>
    </message>
    <message>
        <location filename="../src/components/timeline/tuptimeline.cpp" line="476"/>
        <location filename="../src/components/timeline/tuptimeline.cpp" line="486"/>
        <location filename="../src/components/timeline/tuptimeline.cpp" line="558"/>
        <location filename="../src/components/timeline/tuptimeline.cpp" line="564"/>
        <source>Frame %1</source>
        <translation>Quadro %1</translation>
    </message>
    <message>
        <location filename="../src/components/timeline/tuptimeline.cpp" line="553"/>
        <source>Layer %1</source>
        <translation>Camada %1</translation>
    </message>
    <message>
        <location filename="../src/components/timeline/tuptimeline.cpp" line="613"/>
        <source>Scene %1</source>
        <translation>Cena %1</translation>
    </message>
</context>
<context>
    <name>TupToolStatus</name>
    <message>
        <location filename="../src/components/paintarea/tuptoolstatus.cpp" line="46"/>
        <location filename="../src/components/paintarea/tuptoolstatus.cpp" line="49"/>
        <source>Current Tool</source>
        <translation>Ferramenta Atual</translation>
    </message>
</context>
<context>
    <name>TupToolsDialog</name>
    <message>
        <location filename="../src/components/paintarea/tuptoolsdialog.cpp" line="59"/>
        <source>Pencil</source>
        <translation>Lápis</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuptoolsdialog.cpp" line="67"/>
        <source>Ink</source>
        <translation>Tinta</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuptoolsdialog.cpp" line="75"/>
        <source>Polyline</source>
        <translation>Polilinha</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuptoolsdialog.cpp" line="83"/>
        <source>Ellipse</source>
        <translation>Elípse</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuptoolsdialog.cpp" line="91"/>
        <source>Rectangle</source>
        <translation>Retângulo</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuptoolsdialog.cpp" line="99"/>
        <source>Object Selection</source>
        <translation>Seleção de Objeto</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuptoolsdialog.cpp" line="108"/>
        <source>Nodes Selection</source>
        <translation>Seleção de Contorno</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuptoolsdialog.cpp" line="116"/>
        <source>Color Palette</source>
        <translation>Paleta de Cor</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuptoolsdialog.cpp" line="124"/>
        <source>Pen Size</source>
        <translation>Tamanho da Caneta</translation>
    </message>
    <message>
        <location filename="../src/components/paintarea/tuptoolsdialog.cpp" line="132"/>
        <source>Opacity Value</source>
        <translation>Valor de Opacidade</translation>
    </message>
</context>
<context>
    <name>TupTwitter</name>
    <message>
        <location filename="../src/components/twitter/tuptwitter.cpp" line="270"/>
        <source>Latest version</source>
        <translation type="unfinished">Última versão</translation>
    </message>
    <message>
        <location filename="../src/components/twitter/tuptwitter.cpp" line="271"/>
        <source>Revision</source>
        <translation type="unfinished">Revisão</translation>
    </message>
    <message>
        <location filename="../src/components/twitter/tuptwitter.cpp" line="272"/>
        <source>Code Name</source>
        <translation type="unfinished">Nome de Código</translation>
    </message>
    <message>
        <location filename="../src/components/twitter/tuptwitter.cpp" line="275"/>
        <source>It&apos;s time to upgrade! Click here!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/components/twitter/tuptwitter.cpp" line="279"/>
        <source>Want to help us to make a better project? Click here!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TupTwitterWidget</name>
    <message>
        <location filename="../src/components/twitter/tuptwitterwidget.cpp" line="47"/>
        <source>News!</source>
        <translation>Notícias!</translation>
    </message>
</context>
<context>
    <name>TupViewColorCells</name>
    <message>
        <location filename="../src/components/colorpalette/tupviewcolorcells.cpp" line="111"/>
        <source>Default Palette</source>
        <translation>Paleta Padrão</translation>
    </message>
    <message>
        <location filename="../src/components/colorpalette/tupviewcolorcells.cpp" line="120"/>
        <source>Named Colors</source>
        <translation>Cores Etiquetadas</translation>
    </message>
    <message>
        <location filename="../src/components/colorpalette/tupviewcolorcells.cpp" line="127"/>
        <source>Custom Color Palette</source>
        <translation>Paleta de Cor Personalizada</translation>
    </message>
    <message>
        <location filename="../src/components/colorpalette/tupviewcolorcells.cpp" line="132"/>
        <source>Custom Gradient Palette</source>
        <translation>Paleta de Gradiente Personalizada</translation>
    </message>
    <message>
        <location filename="../src/components/colorpalette/tupviewcolorcells.cpp" line="373"/>
        <source>Add Color</source>
        <translation>Adicionar Cor</translation>
    </message>
    <message>
        <location filename="../src/components/colorpalette/tupviewcolorcells.cpp" line="380"/>
        <source>Remove Color</source>
        <translation>Remover Cor</translation>
    </message>
</context>
<context>
    <name>TupWebHunter</name>
    <message>
        <location filename="../src/libbase/tupwebhunter.cpp" line="101"/>
        <source>Information Temporarily Unavailable</source>
        <translation>Informação Temporariamente Indisponível</translation>
    </message>
</context>
<context>
    <name>TweenManager</name>
    <message>
        <location filename="../src/plugins/tools/common/tweenmanager.cpp" line="56"/>
        <source>Create a new Tween</source>
        <translation>Criar um novo Intermeios</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/common/tweenmanager.cpp" line="131"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/common/tweenmanager.cpp" line="131"/>
        <source>Tween name already exists!</source>
        <translation>Nome do intermeios já existe!</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/common/tweenmanager.cpp" line="179"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/common/tweenmanager.cpp" line="181"/>
        <source>Remove</source>
        <translation>Remover</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/common/tweenmanager.cpp" line="184"/>
        <source>Options</source>
        <translation>Opções</translation>
    </message>
</context>
<context>
    <name>Tweener</name>
    <message>
        <location filename="../src/plugins/tools/position/tweener.cpp" line="161"/>
        <location filename="../src/plugins/tools/position/tweener.cpp" line="359"/>
        <location filename="../src/plugins/tools/position/tweener.cpp" line="363"/>
        <location filename="../src/plugins/tools/position/tweener.cpp" line="768"/>
        <source>Position Tween</source>
        <translation>Intermeios de Posição</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/position/tweener.cpp" line="361"/>
        <source>Shift+W</source>
        <translation>Shift+W</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/tweener.cpp" line="101"/>
        <location filename="../src/plugins/tools/color/tweener.cpp" line="239"/>
        <location filename="../src/plugins/tools/color/tweener.cpp" line="243"/>
        <location filename="../src/plugins/tools/color/tweener.cpp" line="527"/>
        <source>Coloring Tween</source>
        <translation>Intermeios de Cor</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/tweener.cpp" line="157"/>
        <location filename="../src/plugins/tools/color/tweener.cpp" line="164"/>
        <location filename="../src/plugins/tools/color/tweener.cpp" line="403"/>
        <location filename="../src/plugins/tools/compound/tweener.cpp" line="472"/>
        <location filename="../src/plugins/tools/opacity/tweener.cpp" line="377"/>
        <location filename="../src/plugins/tools/position/tweener.cpp" line="537"/>
        <location filename="../src/plugins/tools/rotation/tweener.cpp" line="427"/>
        <location filename="../src/plugins/tools/scale/tweener.cpp" line="418"/>
        <location filename="../src/plugins/tools/shear/tweener.cpp" line="427"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/tweener.cpp" line="157"/>
        <source>Coloring Tween can&apos;t be applied to raster images</source>
        <translation>Intermeios de cor não pode ser aplicado a imagens raster</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/tweener.cpp" line="164"/>
        <source>Coloring Tween can&apos;t be applied to SVG files</source>
        <translation>Intermeios de cor não pode ser aplicado a arquivos SVG</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/tweener.cpp" line="241"/>
        <source>Shift+C</source>
        <translation>Shift+C</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/tweener.cpp" line="403"/>
        <location filename="../src/plugins/tools/compound/tweener.cpp" line="472"/>
        <location filename="../src/plugins/tools/opacity/tweener.cpp" line="377"/>
        <location filename="../src/plugins/tools/position/tweener.cpp" line="537"/>
        <location filename="../src/plugins/tools/rotation/tweener.cpp" line="427"/>
        <location filename="../src/plugins/tools/scale/tweener.cpp" line="418"/>
        <location filename="../src/plugins/tools/shear/tweener.cpp" line="427"/>
        <source>Tween name is missing!</source>
        <translation>Nome do intermeios indefinido!</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/tweener.cpp" line="505"/>
        <location filename="../src/plugins/tools/compound/tweener.cpp" line="533"/>
        <location filename="../src/plugins/tools/compound/tweener.cpp" line="619"/>
        <location filename="../src/plugins/tools/opacity/tweener.cpp" line="480"/>
        <location filename="../src/plugins/tools/position/tweener.cpp" line="654"/>
        <location filename="../src/plugins/tools/rotation/tweener.cpp" line="533"/>
        <location filename="../src/plugins/tools/scale/tweener.cpp" line="523"/>
        <location filename="../src/plugins/tools/shear/tweener.cpp" line="540"/>
        <source>Frame %1</source>
        <translation>Quadro %1</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/tweener.cpp" line="516"/>
        <location filename="../src/plugins/tools/compound/tweener.cpp" line="635"/>
        <location filename="../src/plugins/tools/opacity/tweener.cpp" line="491"/>
        <location filename="../src/plugins/tools/position/tweener.cpp" line="665"/>
        <location filename="../src/plugins/tools/rotation/tweener.cpp" line="544"/>
        <location filename="../src/plugins/tools/scale/tweener.cpp" line="535"/>
        <location filename="../src/plugins/tools/shear/tweener.cpp" line="551"/>
        <source>Info</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/color/tweener.cpp" line="516"/>
        <location filename="../src/plugins/tools/compound/tweener.cpp" line="635"/>
        <location filename="../src/plugins/tools/opacity/tweener.cpp" line="491"/>
        <location filename="../src/plugins/tools/position/tweener.cpp" line="665"/>
        <location filename="../src/plugins/tools/rotation/tweener.cpp" line="544"/>
        <location filename="../src/plugins/tools/scale/tweener.cpp" line="535"/>
        <location filename="../src/plugins/tools/shear/tweener.cpp" line="551"/>
        <source>Tween %1 applied!</source>
        <translation>Intermeios %1 aplicado!</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweener.cpp" line="144"/>
        <location filename="../src/plugins/tools/compound/tweener.cpp" line="379"/>
        <location filename="../src/plugins/tools/compound/tweener.cpp" line="383"/>
        <location filename="../src/plugins/tools/compound/tweener.cpp" line="758"/>
        <source>Compound Tween</source>
        <translation>Intermeios Composto</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweener.cpp" line="381"/>
        <source>Shift+X</source>
        <translation>Shift+X</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/opacity/tweener.cpp" line="117"/>
        <location filename="../src/plugins/tools/opacity/tweener.cpp" line="226"/>
        <location filename="../src/plugins/tools/opacity/tweener.cpp" line="230"/>
        <location filename="../src/plugins/tools/opacity/tweener.cpp" line="502"/>
        <source>Opacity Tween</source>
        <translation>Intermeios de Opacidade</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/opacity/tweener.cpp" line="228"/>
        <source>Shift+O</source>
        <translation>Shift+O</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/rotation/tweener.cpp" line="134"/>
        <location filename="../src/plugins/tools/rotation/tweener.cpp" line="257"/>
        <location filename="../src/plugins/tools/rotation/tweener.cpp" line="261"/>
        <location filename="../src/plugins/tools/rotation/tweener.cpp" line="555"/>
        <source>Rotation Tween</source>
        <translation>Intermeios de Rotação</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/rotation/tweener.cpp" line="259"/>
        <source>Shift+R</source>
        <translation>Shift+R</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scale/tweener.cpp" line="129"/>
        <location filename="../src/plugins/tools/scale/tweener.cpp" line="250"/>
        <location filename="../src/plugins/tools/scale/tweener.cpp" line="254"/>
        <location filename="../src/plugins/tools/scale/tweener.cpp" line="546"/>
        <source>Scale Tween</source>
        <translation>Intermeios de Escala</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/scale/tweener.cpp" line="252"/>
        <source>Shift+S</source>
        <translation>Shift+S</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/shear/tweener.cpp" line="133"/>
        <location filename="../src/plugins/tools/shear/tweener.cpp" line="256"/>
        <location filename="../src/plugins/tools/shear/tweener.cpp" line="260"/>
        <location filename="../src/plugins/tools/shear/tweener.cpp" line="562"/>
        <source>Shear Tween</source>
        <translation>Intermeios de Corte</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/shear/tweener.cpp" line="258"/>
        <source>Shift+H</source>
        <translation>Shift+H</translation>
    </message>
</context>
<context>
    <name>TweenerPanel</name>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="84"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="119"/>
        <source>Options</source>
        <translation>Opções</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="120"/>
        <source>Select object</source>
        <translation>Selecionar objeto</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="121"/>
        <source>Set Tweeners</source>
        <translation>Aplicar Intermediadores</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="145"/>
        <source>Tweeners</source>
        <translation>intermediadores</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="210"/>
        <source>Position</source>
        <translation>Posição</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="210"/>
        <source>Rotation</source>
        <translation>Rotação</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="210"/>
        <source>Scale</source>
        <translation>Escala</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="210"/>
        <source>Shear</source>
        <translation>Corte</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="210"/>
        <source>Opacity</source>
        <translation>Opacidade</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="210"/>
        <source>Coloring</source>
        <translation>Cor</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="275"/>
        <source>Save Tween</source>
        <translation>Salvar Intermeios</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="277"/>
        <source>Cancel Tween</source>
        <translation>Cancelar Intermeios</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="327"/>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="369"/>
        <source>Info</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="327"/>
        <source>Select objects for Tweening first!</source>
        <translation>Selecione os objetos para o Intermeios primeiro!</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="421"/>
        <source>Update Tween</source>
        <translation>Atualizar Intermeios</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenerpanel.cpp" line="423"/>
        <source>Close Tween properties</source>
        <translation>Fechar propriedades de Intermeios</translation>
    </message>
</context>
<context>
    <name>TweenerTable</name>
    <message>
        <location filename="../src/plugins/tools/compound/tweenertable.cpp" line="52"/>
        <source>Position</source>
        <translation>Posição</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenertable.cpp" line="52"/>
        <source>Rotation</source>
        <translation>Rotação</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenertable.cpp" line="52"/>
        <source>Scale</source>
        <translation>Escala</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenertable.cpp" line="52"/>
        <source>Shear</source>
        <translation>Corte</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenertable.cpp" line="52"/>
        <source>Opacity</source>
        <translation>Opacidade</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/compound/tweenertable.cpp" line="52"/>
        <source>Coloring</source>
        <translation>Cor</translation>
    </message>
</context>
<context>
    <name>VideoProperties</name>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="877"/>
        <source>Set Animation Properties</source>
        <translation>Definir Propriedades da Animação</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="889"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="890"/>
        <source>My Video</source>
        <translation>Meu Vídeo</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="895"/>
        <source>Topics</source>
        <translation>Tópicos</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="896"/>
        <source>#topic1 #topic2 #topic3</source>
        <translation>#tópico1 #tópico2 #tópico3</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="901"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="907"/>
        <source>Just a little taste of my style :)</source>
        <translation>Uma pequena amostra do meu estilo :)</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="968"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="988"/>
        <source>Set a title for the picture here!</source>
        <translation>Defina um título para a imagem aqui!</translation>
    </message>
    <message>
        <location filename="../src/components/export/tupexportwidget.cpp" line="975"/>
        <location filename="../src/components/export/tupexportwidget.cpp" line="999"/>
        <source>Set some topic tags for the picture here!</source>
        <translation>Defina algumas etiquetas de tópicos para a imagem aqui!</translation>
    </message>
</context>
<context>
    <name>ViewTool</name>
    <message>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="91"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="96"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="101"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="143"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="184"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="288"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="304"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="334"/>
        <source>Shift</source>
        <translation>Mão</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="91"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="103"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="108"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="145"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="206"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="241"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="263"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="336"/>
        <source>Zoom In</source>
        <translation>Aproximar</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="91"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="110"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="115"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="206"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="245"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="265"/>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="338"/>
        <source>Zoom Out</source>
        <translation>Afastar</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="104"/>
        <source>Z</source>
        <translation>Z</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="111"/>
        <source>Shift+Z</source>
        <translation>Shift+Z</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/hand/viewtool.cpp" line="97"/>
        <source>H</source>
        <translation>H</translation>
    </message>
</context>
<context>
    <name>ZoomConfigurator</name>
    <message>
        <location filename="../src/plugins/tools/hand/zoomconfigurator.cpp" line="54"/>
        <source>Scale Factor</source>
        <translation>Fator de Escala</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/hand/zoomconfigurator.cpp" line="67"/>
        <source>Tips</source>
        <translation>Dicas</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/hand/zoomconfigurator.cpp" line="74"/>
        <source>Zoom Square mode</source>
        <translation>Modo de Ampliação Quadrado</translation>
    </message>
    <message>
        <location filename="../src/plugins/tools/hand/zoomconfigurator.cpp" line="74"/>
        <source>Press Ctrl key + Mouse left button</source>
        <translation>Pressione a tecla Ctrl + o botão esquerdo do Mouse</translation>
    </message>
</context>
</TS>
